#!/usr/bin/env julia

# Dependences management (activate project environment and install packages)
import Pkg
Pkg.activate(Base.current_project())
Pkg.instantiate()

# Convert Pluto notebooks in static HTML
import PlutoSliderServer as PSS
PSS.export_directory("notebooks")

# Switch to site sub-project
cd("site")

# Generate the website
import Franklin as FKN
FKN.optimize(minify = false, prerender = false, clear = true)