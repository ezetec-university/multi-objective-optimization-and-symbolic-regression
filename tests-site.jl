#!/usr/bin/env julia

# Dependences management (activate project environment and install packages)
import Pkg
Pkg.activate(Base.current_project())
Pkg.instantiate()

# Switch to site sub-project
cd("site")

# Run Franklin tests
import Franklin as FKN
FKN.verify_links()