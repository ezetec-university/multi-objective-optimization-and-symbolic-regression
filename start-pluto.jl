#!/usr/bin/env julia

# Dependences management (activate project environment and install packages)
import Pkg

Pkg.activate(Base.current_project())
Pkg.instantiate()

# Load environment variables
using DotEnv

DotEnv.load!()

# Configure and run Pluto
import Pluto

portEnvName = "PLUTO_HOST_PORT"
port = haskey(ENV, portEnvName) ? 
   parse(Int, ENV[portEnvName]) : Pluto.Configuration.PORT_HINT_DEFAULT
conf = Pluto.Configuration.from_flat_kwargs(;
      port = port,
      launch_browser = true,
      show_file_system = false,
      dismiss_update_notification = true,

      # Causes error "UndefVarError: `DEFAULT_PRECEDENCE_HEURISTIC` not defined"
      # run_notebook_on_load = false
   )

Pluto.run(conf)
