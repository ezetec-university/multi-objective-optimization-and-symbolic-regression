### A Pluto.jl notebook ###
# v0.19.46

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 733b2573-753a-453d-be7e-59dfcebcae98
begin
    import Pkg
    # activate the shared project environment
    Pkg.activate(Base.current_project())
    # instantiate, i.e. make sure that all packages are downloaded
    Pkg.instantiate()
end;

# ╔═╡ 7adcb646-f43c-40df-84d1-7a9cad10f181
begin
	# Utilities
	import Printf

	# Dynamic inputs
	import PlutoUI as PUI
	import PlutoUIExtra as PUIE

	# Maths
	import Distributions as DST
	import Optimization as OPT
	import OptimizationOptimJL as OPTJL
	
	# Graphics (frontend e backend)
	import Makie as MK
	import CairoMakie as CMK
	using LaTeXStrings
	
	md"""
	Dependences loaded.
	"""
end

# ╔═╡ cbd87dfa-4d86-11ef-2960-4123987c10c2
md"""
# 1.2 Scalarisation methods
"""

# ╔═╡ 638c883b-c7f6-430c-bcc5-bf05930fab17
md"""
## Notebook set-up
"""

# ╔═╡ 180910cb-ed20-4912-993e-d3fe19372583
begin 
	Base.show(io::IO, f::Float64) = Printf.@printf(io, "%.3f", f)
	CMK.activate!()
	MK.set_theme!(
		MK.Theme(
			size = (1600, 600),
			fonts=(; 
				regular = MK.texfont(:text),
				bold = MK.texfont(:bold),
				italic = MK.texfont(:italic),
				bold_italic = MK.texfont(:bolditalic)),
			font = :regular,
			fontsize = 38,
			Axis = (
				rightspinevisible = false,
				topspinevisible = false,
				xgridvisible = false,
				ygridcolor = :grey90,
				xlabelsize = 38,
				ylabelsize = 38,
				labelsize = 38,
				titlefont = :regular
			)
		)
	)

	contourf_options = (levels = 10, colormap = :greys, )
	contour_options = (levels = contourf_options.levels - 1, labels = true, 
		labelsize = 24, labelcolor = :white, colormap = :greys, )
	lines_f1_utopia_options = (color = :teal, linewidth = 8, )
	scatter_options = (color = :black, markersize = 3, )
	scatter_f2_utopia_options = (color = :cyan, markersize = 18, )
	scatter_fopt_options = (colormap = :winter, markersize = 18, )
	tooltip_options = (fontsize = 24, )
	vlines_options = (linestyle = :dashdot, )

	md"""
	Configurations applied.
	"""
end

# ╔═╡ 856d274c-c6fa-4e97-86d3-33fc3a913182
let
	toc_ui = PUI.TableOfContents(depth = 4)
	md"""
	Table of contents added.
	$(toc_ui)
	"""
end

# ╔═╡ d3fbffba-6ed0-4bf8-a314-c9da1506505b
let
	# Optimisation problem 
	opt_prob_combined_ui = PUI.combine() do Child
		x0_min_ui = Child("x0_min", 
			PUI.Slider((-5.0:0.1:5.0), default = -0.1, show_value = true))
		x0_max_ui = Child("x0_max", 
			PUI.Slider((-5.0:0.1:5.0), default = 1.0, show_value = true))
		x1_min_ui = Child("x1_min", 
			PUI.Slider((-5.0:0.1:5.0), default = -0.1, show_value = true))
		x1_max_ui = Child("x1_max", 
			PUI.Slider((-5.0:0.1:5.0), default = 1.0, show_value = true))
		a_ui = Child("a", 
			PUI.Slider((-5.0:0.1:5.0), default = -0.5, show_value = true))
		b_ui = Child("b", 
			PUI.Slider((-5.0:0.1:5.0), default = 2.5, show_value = true))

		md"""
		**Optimisation problem**\
		Solution domain\
		``\underline{x_0}`` $(x0_min_ui)\
		``\overline{x_0}`` $(x0_max_ui)\
		``\underline{x_1}`` $(x1_min_ui)\
		``\overline{x_1}`` $(x1_max_ui)\
		Parameters of the second objective\
		function, ``f_2``:\
		``a`` $(a_ui) \
		``b`` $(b_ui)
		"""
	end

	# Analyses 
	analyses_combined_ui = PUI.combine() do Child
		x_len_ui = Child("len", 
			PUI.Slider(10:1000, default = 500, show_value = true))
		md"""
		**Analyses**\
		Sample points\
		n. $(x_len_ui)
		"""
	end
	
	# Show the sidebar
	sidebar_ui = PUIE.Sidebar(
		(@bind opt_prob_spec PUI.confirm(opt_prob_combined_ui, label = "Apply")),
		md"""
		\
		""",
		(@bind analyses_spec PUI.confirm(analyses_combined_ui, label = "Apply"));
		location = "lower right"
	)
	md"""
	Sidebar with dynamic configurations added.
	$(sidebar_ui)
	"""
end

# ╔═╡ efdd67dd-45f7-4780-a87a-eade5bd4e6b6
md"""
## Optimisation problem set-up
"""

# ╔═╡ 617fb846-3cf0-4af6-aafd-087721b56a9f
md"""
Define the objective functions to be minimised
"""

# ╔═╡ e95e4249-605a-4ce1-b1d3-e9100a393ff4
"""
    f1(x::AbstractVector)

Returns the value of the objective function 1 given the solution vector ``\\mathbf{x} = \\begin{pmatrix}x_0 & \\cdots & x_n \\end{pmatrix}^\\intercal``:

```math
f_1(\\mathbf{x}) = x_0
```

**Arguments**\\
`x` Solution [``x_0``, ``x_1``]
"""
function f1(
	x::AbstractVector
)
	return x[1]
end 

# ╔═╡ 22d2a3e9-6745-4e33-88fb-55f7a5fc8ea5
"""
    generate_f2(a::Number,b::Number)

Returns an instance of the objective function 2, which returns its value given the solution vector ``\\mathbf{x} = \\begin{pmatrix}x_0 & \\cdots & x_n \\end{pmatrix}^\\intercal``:

```math
f_2(\\mathbf{x}) = 1 - x_0 - a \\sin\\left(b \\pi x_0\\right) + x_1^2
```

**Arguments**\\
`a`, `b` Objectives parameters
"""
function generate_f2(
	a::Number,
	b::Number
)::Function

	function f2(
		x::AbstractVector
	)
		return 1 - x[1] - (a * sin(b * π * x[1])) + (x[2]^2)
	end 
	
	return f2
end

# ╔═╡ 4cf8f36d-5899-4600-af28-569fe879a395
begin
	f2_spec_convex = (a = 0.2, b = 1.0)
	
	"""
	    f2_c(x::AbstractVector)
	
	Convex instance of the objective function 2
	
	```math
	f_{2_\\text{c}}(\\mathbf{x}) = \\left. f_2(\\mathbf{x}, a, b) \\right|_{
		(a = $(f2_spec_convex.a), \\, b = $(f2_spec_convex.b))} 
	```
	
	**Arguments**\\
	`x` Solution [``x_0``, ``x_1``]
	"""
	f2_c = generate_f2(f2_spec_convex.a, f2_spec_convex.b)
end

# ╔═╡ 6a491ff3-8728-43bb-84c4-7e6f0a72bcbe
begin
	f2_spec_nonconvex = (a = 0.1, b = 3.0)
	
	"""
	    f2_nc(x::AbstractVector)
	
	Non-convex instance of the objective function 2
	
	```math
	f_{2_\\text{nc}}(\\mathbf{x}) = \\left. f_2(\\mathbf{x}, a, b) \\right|_{
		(a = $(f2_spec_nonconvex.a), \\, b = $(f2_spec_nonconvex.b))} 
	```
	
	**Arguments**\\
	`x` Solution [``x_0``, ``x_1``]
	"""
	f2_nc = generate_f2(f2_spec_nonconvex.a, f2_spec_nonconvex.b)
end

# ╔═╡ d12ef486-2bda-4abf-ba75-f063d4feafba
"""
    f2_u(x::AbstractVector)

User instance of the objective function 2

```math
f_{2_\\text{u}}(\\mathbf{x}) = \\left. f_2(\\mathbf{x}, a, b) \\right|_{
	(a = $(opt_prob_spec.a), \\, b = $(opt_prob_spec.b))} 
```

**Arguments**\\
`x` Solution [``x_0``, ``x_1``]
"""
f2_u= generate_f2(opt_prob_spec.a, opt_prob_spec.b)

# ╔═╡ 802d187a-f75a-4d5f-b1ab-175a7635846e
begin
	x0_dist = DST.Uniform(getfield.(Ref(opt_prob_spec),[:x0_min, :x0_max])...)
	x1_dist = DST.Uniform(getfield.(Ref(opt_prob_spec),[:x1_min, :x1_max])...)
	x = vec([rand.([x0_dist, x1_dist]) 
		for idx ∈ 1:analyses_spec.len])

	md"""
	For the upcoming analyses, $(analyses_spec.len) points are uniformely and randomly sampled from the solution space within the solution domain.
	"""
end

# ╔═╡ 86a6579e-4a95-4be6-8ff9-335b746353e2
"""
    make_outcomespace_basefigure()::AbstractVector

Utility function to generate the base figure with the three axes for the convex, non-convex and user scenarios.
"""
function make_outcomespace_basefigure()::AbstractVector
	fig = MK.Figure()

	subtitle(f2_spec) = L"a = %$(f2_spec.a) \quad b = %$(f2_spec.b)"
	
	axes = [
		MK.Axis(fig[1, 1],
			title = L"Convex $f_{2_\text{c}}$",
			subtitle = subtitle(f2_spec_convex),
			xlabel = L"f_1(\mathbf{x})",
			ylabel = L"f_2(\mathbf{x})",
			aspect = MK.DataAspect()),
		MK.Axis(fig[1, 2],
			title = L"Non-convex $f_{2_\text{nc}}$",
			subtitle = subtitle(f2_spec_nonconvex),
			xlabel = L"f_1(\mathbf{x})",
			aspect = MK.DataAspect()),
		MK.Axis(fig[1, 3],
			title = L"User $f_{2_\text{u}}$",
			subtitle = subtitle(opt_prob_spec),
			xlabel = L"f_1(\mathbf{x})",
			aspect = MK.DataAspect())]
	
	# Enforce same axis limits
	MK.linkxaxes!(axes...)
	MK.linkyaxes!(axes...)

	# Enforce the correct aspect
	f1_extrema = extrema(f1.(x))
	f2_extrema = extrema([f2_c.(x); f2_nc.(x); f2_u.(x)])
	aspect = -(f2_extrema...)[1] /  -(f1_extrema...)[1]
	MK.rowsize!(fig.layout, 1, MK.Aspect(1.0, aspect))
	
	return [fig, axes]
end

# ╔═╡ b37c699c-0c6c-479a-969b-c1064c82fa3d
"""
    draw_random_outcomes(axes::AbstractVector{MK.Axis})::Nothing

Utility function to add randome outcomes to a figure with the three axes for the convex, non-convex and user scenarios.
"""
function draw_random_outcomes(
	axes::AbstractVector{MK.Axis}
)
	MK.scatter!(axes[1], f1.(x), f2_c.(x); scatter_options...)
	MK.scatter!(axes[2], f1.(x), f2_nc.(x); scatter_options...)
	MK.scatter!(axes[3], f1.(x), f2_u.(x); scatter_options...)

	return Nothing
end

# ╔═╡ 887d96fb-7655-4b82-8095-e93b90d33846
let
	fig, axes = make_outcomespace_basefigure()
	draw_random_outcomes(axes)

	# Title axis
	MK.Label(fig[0, :], "Outcomes from uniformly and randomly sampled solutions")

	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ cbf25634-a447-41b3-91e6-d5d7369e1788
md"""
### Utopia points
"""

# ╔═╡ b92bccea-c413-4045-9a32-2cfceda4bc89
md"""
The **utopia points**, a term coined in [Yu 1973 - A Class of Solutions for Group Decision Problems](https://doi.org/10.1287/mnsc.19.8.936), are the points the individually optimise each objective function. If the solution domain ``\mathcal{D} = \mathcal{R}^2`` it is easy to see that the utopia solutions are:

```math
\begin{aligned}
\mathbf{x}_1^* &=
\min_{\mathbf{x}} f_1(\mathbf{x}) = 
\begin{pmatrix} 
-\infty \\
\cdot
\end{pmatrix} 
\\
\mathbf{x}_2^* &= 
\min_{\mathbf{x}} f_2(\mathbf{x}) = 
\begin{pmatrix} 
+\infty \\
0
\end{pmatrix}
\end{aligned}
\quad ,
```

where the ``\cdot`` notation is used to denote that any possible value applies. The two objective functions conflict on the first component of the solution space, ``x_0``, while the second component, ``x_1``, is only relevant to the second objective function. 
"""

# ╔═╡ edbee35a-9603-4da8-b6e4-65c61e0e721d
md"""
The problem becomes more interesting when the solution space is limited, namely ``\mathbf{x} \in \mathcal{D} \subset \mathcal{R}^2``. In this instance, the utopia points are identified through the analysis of the objective function's local minima within the solution space:

```math
\mathcal{D} = \left\{
\begin{pmatrix} x_0 & x_1 \end{pmatrix}^\intercal
\mid
x_0 \in \mathcal{D}_0 = [\underline{x_0}, \overline{x_0}],
x_1 \in \mathcal{D}_1 = [\underline{x_1}, \overline{x_1}]
\right\}
\neq \varnothing
```

The result is:

```math
\begin{aligned}
\mathbf{x}_1^* &=
\min_{\mathbf{x} \in \mathcal{D}} f_1(\mathbf{x}) = 
\begin{pmatrix} 
	\underline{\mathbf{x}_0}
	\\
	\cdot
\end{pmatrix}
\\
\mathbf{x}_2^* &=
\min_{\mathbf{x} \in \mathcal{D}} f_2(\mathbf{x}) = 
\begin{cases}
\begin{pmatrix} 
	\max \left(\text{sign}(a) \frac{1}{b\pi} \arccos \left(-\frac{1}{ab\pi}\right) 
		+ \frac{2}{b} k\right) 
	\\
	x_{2_1}^*
\end{pmatrix}
&: \left|\frac{1}{ab\pi}\right| \le 1
\\
\begin{pmatrix} 
	\overline{\mathbf{x}_0}
	\\
	x_{2_1}^* 
\end{pmatrix}
&: \left|\frac{1}{ab\pi}\right| > 1
\end{cases}
\end{aligned}
\quad ,
```

where ``k \in \mathcal{Z}`` and ``x_{2_1}^* \in \mathcal{D}_1 \: \mid \: \left| x_{2_1}^* - 0 \right| \le \left|x_1 - 0\right| \: \forall \, x_1 \in \mathcal{D}_1 ``.
"""

# ╔═╡ 75b7a736-c837-4e17-b3e9-022768a6acca
"""
    generate_f1_utopia_solution(x_extrema::NTuple{4, Number})

Returns a functions that computes the solution whose image is the utopia point:

```math
\\mathbf{x}_1^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1})
=
\\begin{pmatrix} 
	\\underline{x_0}
	\\\\
	\\text{nothing}
\\end{pmatrix}
```

**Arguments**\\
`x_estrema` Solution domain (``\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}``)
"""
function generate_f1_utopia_solution(
	x_extrema::NTuple{4, Number}
)
	function f1_utopia_solution() 
		x0_opt = x_extrema[1]
		x1_opt = nothing
		return [x0_opt, x1_opt]
	end
	
	return f1_utopia_solution
end

# ╔═╡ bd70fd5d-3191-48ca-a563-106111873153
"""
    f1_utopia_solution()

Instance function that returns the utopia solution of objective function 1 within the solution domain

```math
\\mathbf{x}_1^*() = 
\\left.
\\mathbf{x}_1^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1})
\\right|_{
	\\begin{align}
		\\underline{x_0} &= $(opt_prob_spec.x0_min) \\\\
		\\overline{x_0} &= $(opt_prob_spec.x0_max) \\\\
		\\underline{x_1} &= $(opt_prob_spec.x1_min) \\\\
		\\overline{x_1} &= $(opt_prob_spec.x1_max)
	\\end{align}
}
```
"""
f1_utopia_solution = generate_f1_utopia_solution(
	getfield.(Ref(opt_prob_spec), (:x0_min, :x0_max, :x1_min, :x1_max)))

# ╔═╡ 55504181-293c-4869-8e15-f4e35630a0d3
"""
    generate_f2_utopia_solution(x_extrema::NTuple{4, Number})

Returns a functions that computes the solution within the solution domain whose image is the utopia point given the optimization problem parameters ``a`` and ``b``:

```math
\\begin{gather}
\\mathbf{x}_2^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, a, b) =
\\begin{cases}
\\begin{pmatrix} 
\\max \\left(\\text{sign}(a) \\frac{1}{b\\pi} \\arccos \\left(-\\frac{1}{ab\\pi}\\right) + \\frac{2}{b} k\\right) \\\\
x_{1_2}^*
\\end{pmatrix}
&: \\left| \\frac{1}{ab\\pi} \\right| \\le 1\\\\
\\begin{pmatrix}
\\overline{x_0} \\\\
x_{1_2}^*
\\end{pmatrix}
&: \\left|\\frac{1}{ab\\pi}\\right| > 1
\\end{cases}
\\\\
k \\in \\mathcal{Z} 
\\qquad 
x_{1_2}^* \\in \\mathcal{D}_1 
\\qquad 
\\left| x_{1_2}^* - 0 \\right| \\le \\left|x_1 - 0\\right| \\: \\forall \\, x_1 \\in \\mathcal{D}_1
\\end{gather}
```

**Arguments**\\
`x_estrema` Solution domain (``\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}``)
"""
function generate_f2_utopia_solution(
	x_extrema::NTuple{4, Number}
)
	function f2_utopia_solution(
		a::Number,
		b::Number
	)
		acos_arg = -1/(a*b*π)
		x0_opt = if abs(acos_arg) > 1 
			x_extrema[2]
		else 
			# Identify all local minima at least within the solution domain
			local_min_down = range(
				start = sign(a) * acos(acos_arg)/(b*π),
				step = - 2/b,
				stop = x_extrema[1])
			local_min_up = range(
				start = sign(a) * acos(acos_arg)/(b*π),
				step = 2/b,
				stop = x_extrema[2])
			# Ensure no minima is outside the solution domain
			all_local_min = filter(
				x -> (x >= x_extrema[1]) && (x <= x_extrema[2]),
				[local_min_down; local_min_up])
			# Keep the largest one if there is at least one ...
			if length(all_local_min) > 0
				maximum(all_local_min)
			# otherwise select either one of the two extrema
			else
				f2 = generate_f2(a, b)
				argmin(x -> f2([x, zero(x)]), [x_extrema[1], x_extrema[2]]) 
			end
		end
		x1_opt = if sign(x_extrema[3] * x_extrema[4]) == 1.0
			# Get the extrema that is the nerest to zero
			argmin(x -> abs(x - zero(x)), x_extrema[3:4]) 
		else
			zero(x_extrema[3])
		end		
		return [x0_opt, x1_opt]
	end
	
	return f2_utopia_solution
end

# ╔═╡ 24365b84-a680-4e7e-ad9b-2caff8158948
"""
    f2_utopia_solution(a::Number, b::Number)

Instance functions that returns the utopia solution of objective function 2 within the solution domain:

```math
\\mathbf{x}_2^*(a, b) = 
\\mathbf{x}_2^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, a, b)
\\Bigr|_{
	\\begin{align}
		\\underline{x_0} &= $(opt_prob_spec.x0_min) \\\\
		\\overline{x_0} &= $(opt_prob_spec.x0_max) \\\\
		\\underline{x_1} &= $(opt_prob_spec.x1_min) \\\\
		\\overline{x_1} &= $(opt_prob_spec.x1_max)
	\\end{align}
}
```

**Arguments**\\
`a`, `b` Objectives parameters
"""
f2_utopia_solution = generate_f2_utopia_solution(
	getfield.(Ref(opt_prob_spec), (:x0_min, :x0_max, :x1_min, :x1_max)))

# ╔═╡ 4cce56cd-6ebc-4b27-b6a9-c5adaf1c6fef
begin
	x1_opt = f1_utopia_solution()
	x2_c_opt = f2_utopia_solution(f2_spec_convex.a, f2_spec_convex.b)
	x2_nc_opt = f2_utopia_solution(f2_spec_nonconvex.a, f2_spec_nonconvex.b)
	x2_u_opt = f2_utopia_solution(opt_prob_spec.a, opt_prob_spec.b)

	f1_opt = f1(x1_opt)
	f2_c_opt = f2_c(x2_c_opt)
	f2_nc_opt = f2_nc(x2_nc_opt)
	f2_u_opt = f2_u(x2_u_opt)
	
	Markdown.parse("""
		The theoretical individual optimal solutions, or utopia solutions, are computed for the convex, non-convex and user-defined cases:
		
		```math
		\\begin{aligned}
		\\mathbf{x}_1^* = \\begin{pmatrix}
		$(x1_opt[1]) \\\\
		$(x1_opt[2]) 
		\\end{pmatrix} 
		&&
		\\mathbf{x}_{2_\\text{c}}^* = \\begin{pmatrix}
		$(x2_c_opt[1]) \\\\
		$(x2_c_opt[2]) 
		\\end{pmatrix} 
		&&
		\\mathbf{x}_{2_\\text{nc}}^* = \\begin{pmatrix}
		$(x2_nc_opt[1]) \\\\
		$(x2_nc_opt[2]) 
		\\end{pmatrix} 
		&&
		\\mathbf{x}_{2_\\text{u}}^* = \\begin{pmatrix}
		$(x2_u_opt[1]) \\\\
		$(x2_u_opt[2]) 
		\\end{pmatrix} 
		\\end{aligned}
		```
		
		The corresponding outcomes are:
		
		```math
		\\begin{aligned}
		\\mathbf{f}_\\text{c}(\\mathbf{x}_{2_\\text{c}}^*) = \\begin{pmatrix}
		$(f1(x2_c_opt)) \\\\
		$(f2_c_opt)
		\\end{pmatrix}
		&&
		\\mathbf{f}_\\text{nc}(\\mathbf{x}_{2_\\text{nc}}^*) = \\begin{pmatrix}
		$(f1(x2_nc_opt)) \\\\
		$(f2_nc_opt) 
		\\end{pmatrix} 
		&&
		\\mathbf{f}_\\text{u}(\\mathbf{x}_{2_\\text{u}}^*) = \\begin{pmatrix}
		$(f1(x2_u_opt)) \\\\
		$(f2_u_opt) 
		\\end{pmatrix} 
		\\end{aligned}
		```
		""")
end

# ╔═╡ 1f938bea-811b-4f79-8743-5f07bb62973a
begin
	fig = MK.Figure()

	x0_min, x0_max, x1_min, x1_max = 
		getfield.(Ref(opt_prob_spec), [:x0_min, :x0_max, :x1_min, :x1_max])
	solutions = vec(
		[[x0, x1] for x0 ∈ x0_min:0.01:x0_max, x1 ∈ x1_min:0.01:x1_max])
	contour_arguments_xy = (getindex.(solutions, 1), getindex.(solutions, 2))

	x1_opts = vec([[x0, x1] for
		x0 = x1_opt[1], 
		x1 ∈ range(x1_min, x1_max, length = analyses_spec.len)])

	# Top - f1
	axis = MK.Axis(fig[1, 1:3],
		subtitle = L"f_1",
		xlabel = L"x_0",
		ylabel = L"x_1")
	MK.contourf!(axis, contour_arguments_xy..., f1.(solutions); 
		contourf_options...)	
	MK.contour!(axis, contour_arguments_xy..., f1.(solutions); 
		contour_options...)
	MK.lines!(axis, MK.Point2f.(x1_opts);
		lines_f1_utopia_options...)
	MK.tooltip!(axis, x1_opts[1][1], (x1_min + x1_max)/2, 
		L"(%$(x1_opts[1][1]), \cdot)";
		placement = :right, tooltip_options...)

	# Centre left - f2 convex
	axis = MK.Axis(fig[2, 1],
		title = L"Convex $f_{2_\text{c}}$",
		subtitle = L"a = %$(f2_spec_convex.a) \quad b = %$(f2_spec_convex.b)",
		xlabel = L"x_0",
		ylabel = L"x_1")
	MK.contourf!(axis, contour_arguments_xy..., f2_c.(solutions); 
		contourf_options...)	
	MK.contour!(axis, contour_arguments_xy..., f2_c.(solutions); 
		contour_options...)
	MK.scatter!(axis, x2_c_opt...;
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, x2_c_opt..., 
		"($(x2_c_opt[1]), $(x2_c_opt[2]))";
		align = (x2_c_opt[2] == x1_max) ? 1.0 : 0.0, 
		placement = (x2_c_opt[1] == x0_min) ? :right : :left, 
		tooltip_options...)

	# Centre centre - f2 non-convex
	axis = MK.Axis(fig[2, 2],
		title = L"Non-convex $f_{2_\text{nc}}$",
		subtitle = L"a = %$(f2_spec_nonconvex.a) \quad b = %$(f2_spec_nonconvex.b)",
		xlabel = L"x_0")
	MK.contourf!(axis, contour_arguments_xy..., f2_nc.(solutions); 
		contourf_options...)	
	MK.contour!(axis, contour_arguments_xy..., f2_nc.(solutions); 
		contour_options...)
	MK.scatter!(axis, x2_nc_opt...;
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, x2_nc_opt..., 
		"($(x2_nc_opt[1]), $(x2_nc_opt[2]))";
		align = (x2_nc_opt[2] == x1_max) ? 1.0 : 0.0,
		placement = (x2_nc_opt[1] == x0_min) ? :right : :left, 
		tooltip_options...)

	# Centre right - f2 user-defined
	axis = MK.Axis(fig[2, 3],
		title = L"User-defined $f_{2_\text{u}}$",
		subtitle = L"a = %$(opt_prob_spec.a) \quad b = %$(opt_prob_spec.b)",
		xlabel = L"x_0")
	MK.contourf!(axis, contour_arguments_xy..., f2_u.(solutions); 
		contourf_options...)	
	MK.contour!(axis, contour_arguments_xy..., f2_u.(solutions); 
		contour_options...)
	MK.scatter!(axis, x2_u_opt...;
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, x2_u_opt..., 
		"($(x2_u_opt[1]), $(x2_u_opt[2]))";
		align = (x2_u_opt[2] == x1_max) ? 1.0 : 0.0, 
		placement = (x2_u_opt[1] == x0_min) ? :right : :left, 
		tooltip_options...)
	
	# Title axis
	MK.Label(fig[-1, :], 
		"Objective functions and their utopia solutions within the solution domain")

	MK.rowsize!(fig.layout, 1, MK.Aspect(3, 1))
	MK.rowsize!(fig.layout, 2, MK.Aspect(3, 1))
	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ 0bcddce6-92d5-4401-9ab6-65c0fcecac52
let
	x0_min, x0_max = getfield.(Ref(opt_prob_spec), [:x0_min, :x0_max])
	x1_min, x1_max = getfield.(Ref(opt_prob_spec), [:x1_min, :x1_max])
	x1_opts = vec([[x0, x1] for
		x0 = x1_opt[1], 
		x1 ∈ range(x1_min, x1_max, length = analyses_spec.len)])
	
	fig, axes = make_outcomespace_basefigure()
	draw_random_outcomes(axes)

	# Left - Convex
	axis = axes[1]
		
	MK.lines!(axis, f1.(x1_opts), f2_c.(x1_opts); 
		lines_f1_utopia_options...)
	MK.scatter!(axis, f1(x2_c_opt), f2_c_opt;
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, f1_opt[1], f2_c([x1_opts[1][1], (x1_min + x1_max)/2]), 
		L"(%$(f1_opt[1]), \cdot)";
		placement = :right, tooltip_options...)
	MK.tooltip!(axis, f1(x2_c_opt), f2_c_opt, 
		"($(f1(x2_c_opt)), $(f2_c_opt))";
		align = (f2_c_opt == x1_max) ? 1.0 : 0.0, 
		placement = (f1(x2_c_opt) == x0_min) ? :right : :left, 
		tooltip_options...)
	
	# Center - Non-convex
	axis = axes[2]

	MK.lines!(axis, f1.(x1_opts), f2_nc.(x1_opts); 
		lines_f1_utopia_options...)
	MK.scatter!(axis, f1(x2_nc_opt), f2_nc(x2_nc_opt);
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, f1_opt[1], f2_nc([x1_opts[1][1], (x1_min + x1_max)/2]), 
		L"(%$(f1_opt[1]), \cdot)";
		placement = :right, tooltip_options...)
	MK.tooltip!(axis, f1(x2_nc_opt), f2_nc_opt, 
		"($(f1(x2_nc_opt)), $(f2_nc_opt))";
		align = (f2_nc_opt == x1_max) ? 1.0 : 0.0,
		placement = (f1(x2_nc_opt) == x0_min) ? :right : :left, 
		tooltip_options...)

	# Right - User-defined
	axis = axes[3]
	
	MK.lines!(axis, f1.(x1_opts), f2_u.(x1_opts); 
		lines_f1_utopia_options...)
	MK.scatter!(axis, f1(x2_u_opt), f2_u_opt;
		scatter_f2_utopia_options...)
	MK.tooltip!(axis, f1_opt[1], f2_u([x1_opts[1][1], (x1_min + x1_max)/2]), 
		L"(%$(f1_opt[1]), \cdot)";
		placement = :right, tooltip_options...)
	MK.tooltip!(axis, f1(x2_u_opt), f2_u_opt, 
		"($(f1(x2_u_opt)), $(f2_u_opt))";
		align = (f2_u_opt == x1_max) ? 1.0 : 0.0, 
		placement = (f1(x2_u_opt) == x0_min) ? :right : :left, 
		tooltip_options...)
	
	# Title axis
	MK.Label(fig[0, :], 
		"Objective functions and their utopia points within the outcome domain")
	
	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ 308e84e5-7faf-422f-99fe-ef2f01b92c9d
md"""
## Objectives scalarisation methods
"""

# ╔═╡ fec802f6-fe8f-4c44-8874-4c91bcb65b03
md"""
### Weighted sum model
"""

# ╔═╡ 9b09a8c7-896f-4910-8c78-06f1f8bb195d
md"""
The weigthed sum model replaces the multiple objective functions with a single one defined by

```math
f_s(\mathbf{x}) = \sum_j w_j f_j(\mathbf{x}) \quad ,
```

where ``w_j`` are weights. In this analysis, the weighted sum model takes form:

```math
f_s(\mathbf{x}) = w_2 + \left (w_1 - w_2 \right) x_0  - w_2 a \sin\left(b \pi x_0\right) + w_2 x_1^2 \quad .
```
"""

# ╔═╡ a94a30f7-c518-4836-9610-0b01d80b89ef
"""
    generate_fs(a::Number, b::Number)

Returns an instance of the weighted sum objective function, which returns its value given the solution vector ``\\mathbf{x} = \\begin{pmatrix}x_0 & \\cdots & x_n \\end{pmatrix}^\\intercal`` and weights ``w_1`` and ``w_2``:

```math
f_s(\\mathbf{x}, w_1, w_2) = w_1 f_1(\\mathbf{x}) + w_2 f_2(\\mathbf{x}, a, b) \\Bigr|_{\\substack{a\\\\b}}
```

**Arguments**\\
`a`, `b` Objectives parameters
"""
function generate_fs(
	a::Number,
	b::Number
)
	f2 = generate_f2(a, b)
	function fs(
		x::AbstractVector,
		w1::Number,
		w2::Number
	)
		return (w1 * f1(x)) + (w2 * f2(x))
	end
	
	return fs
end

# ╔═╡ 1897e944-ee51-4646-9904-681252dc63fb
"""
    fs_c(x::AbstractVector, w1::Number,	w2::Number)

Convex instance of the weighted sum objective function:

```math
f_{s_\\text{nc}}(\\mathbf{x}, w_1, w_2) 
= 
f_s (\\mathbf{x}, a, b, w_1, w_2) \\Bigr|_{
	\\substack{
		a = $(f2_spec_convex.a)\\\\
		b = $(f2_spec_convex.b)}} 
```

**Arguments**\\
`x` Solution [``x_0``, ``x_1``]\\
`w1`, `w2` Weighted sum model weights
"""
fs_c = generate_fs(f2_spec_convex.a, f2_spec_convex.b)

# ╔═╡ cc1fd63d-2af4-4157-8dfc-7a51ee33b354
"""
    fs_nc(x::AbstractVector, w1::Number, w2::Number)

Non-convex instance of the weighted sum objective function:

```math
f_{s_\\text{nc}}(\\mathbf{x}, w_1, w_2) 
= 
f_s (\\mathbf{x}, a, b, w_1, w_2) \\Bigr|_{
	\\substack{
		a = $(f2_spec_nonconvex.a)\\\\
		b = $(f2_spec_nonconvex.b)}} 
```

**Arguments**\\
`x` Solution [``x_0``, ``x_1``]\\
`w1`, `w2` Weighted sum model weights
"""
fs_nc = generate_fs(f2_spec_nonconvex.a, f2_spec_nonconvex.b)


# ╔═╡ 5cd00774-458f-4c14-9d11-568f6d580479
"""
    fs_u(x::AbstractVector, w1::Number, w2::Number)

User instance of the weighted sum objective function:

```math
f_{s_\\text{u}}(\\mathbf{x}) = \\left. f_s (\\mathbf{x}, a, b) \\right|_{
	(a = $(opt_prob_spec.a),\\, b = $(opt_prob_spec.b))} 
```

**Arguments**\\
`x` Solution [``x_0``, ``x_1``]\\
`w1`, `w2` Weighted sum model weights
"""
fs_u = generate_fs(opt_prob_spec.a, opt_prob_spec.b)

# ╔═╡ 7dbff727-c994-407b-8b40-fc74486c8d89
md"""
#### Theoretical optimal solution
"""

# ╔═╡ c412998e-456f-48c7-8722-7fd65f31fe8a
md"""
In the case where the solution domain is ``\mathcal{D} = \mathcal{R}^2``, it is evident that the optimal solution is straightforward. In this scenario, the conflict pertaining to the first component of the solution, ``x_0``, is resolved through the relative importance of the weighted sum model weights:

```math
\mathbf{x}_s^* =
\min_{\mathbf{x}} f_s(\mathbf{x}) = 
\begin{cases}
\begin{pmatrix} 
-\infty \\
0
\end{pmatrix}
&: w_1 > w_2
\\
\begin{pmatrix} 
+\infty \\
0
\end{pmatrix}
&: w_1 < w_2
\\
\begin{pmatrix} 
\frac{\text{sign}(a)}{2b} + \frac{2}{b} k\\
0
\end{pmatrix}
&: w_1 = w_2 = \frac{1}{2}
\end{cases}
\quad .
```

Note how there is no unique minimum solution when ``w_1 = w_2``. 
"""

# ╔═╡ deca5a7d-65f6-4be2-a35f-f7cb04264e57
md"""
Again, the problem becomes more interesting when the solution space is limited, namely ``\mathbf{x} \in \mathcal{D} \subset \mathcal{R}^2``. Given that the objective function is smooth, the optimal point ``\mathbf{x}^* = \arg\min_{\mathbf{x}} f_s(\mathbf{x})`` is obtained through differentiation and then solving:

```math
\begin{gather}
\left. \frac{\partial f_s(\mathbf{x})}{\partial \mathbf{x}}\right|_{\mathbf{x} = \mathbf{x}^*}
=
\begin{pmatrix}
w_1 - w_2 - w_2 a b \pi \cos\left(b \pi x_0^*\right) \\
2 w_2 x_1^*
\end{pmatrix}
= 0 \\
\left. \frac{\partial^2 f_s(\mathbf{x})}{\partial \mathbf{x}^2} \right|_{\mathbf{x} = \mathbf{x}^*} 
=
\begin{bmatrix}
w_2 a b^2 \pi^2 \sin\left(b \pi x_0^*\right)  & 0 \\
0  & 2 w_2
\end{bmatrix}
> 0
\end{gather}
\quad .
```
"""

# ╔═╡ 1d507ad2-5641-4357-9931-c55da7ce3212
md"""
The solution to the first derivative provides both local minima and local maxima.
The local maxima can be excluded substituting into and solving the second derivative. The result is:  

```math
\mathbf{x}^*
=
\begin{cases}
\begin{pmatrix}
\max \left( \frac{\text{sign}(a)}{b \pi} \arccos \left(\frac{w_1 - w_2}{w_2 a b \pi}\right) + \frac{2}{b}k \right) \\
x_1^*
\end{pmatrix}
& : \left| \frac{w_1 - w_2}{w_2 a b \pi} \right| \le 1
\\
\begin{pmatrix}
\underline{x_0} 
\\
x_1^*
\end{pmatrix}
& : \left| \frac{w_1 - w_2}{w_2 a b \pi} \right| \gt 1, \: w_1 > w_2
\\
\begin{pmatrix}
\overline{x_0} 
\\
x_1^*
\end{pmatrix}
& : \left| \frac{w_1 - w_2}{w_2 a b \pi} \right| \gt 1, \: w_1 < w_2
\end{cases}
\quad ,
```
where ``k \in \mathcal{Z}`` and ``x_1^* \in \mathcal{D}_1 \: \mid \: \left| x_1^* - 0 \right| \le \left|x_1 - 0\right| \: \forall \, x_1 \in \mathcal{D}_1 ``.
"""

# ╔═╡ d341743d-0333-41a7-b545-a340e697e8ce
"""
    generate_xopt(x_extrema::NTuple{4, Number}, a::Number, b::Number)

Returns an instance of the minimisation function, which returns the optimal solution ``\\mathbf{x}^* = \\arg\\min_{\\mathbf{x}}f_s(\\mathbf{x})`` given the weights ``w_1`` and ``w_2``, :

```math
\\begin{gather}
\\mathbf{x}^*(\\substack{
	\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, 
	\\\\
	a, b, w_1, w_2})
=
\\begin{cases}
\\begin{pmatrix}
\\max \\left(\\frac{\\text{sign}(a)}{b \\pi} \\arccos \\left(\\frac{w_1 - w_2}{w_2 a b \\pi}\\right) + \\frac{2}{b}k \\right) \\\\
x_1^*
\\end{pmatrix}
& : \\left| \\frac{w_1 - w_2}{w_2 a b \\pi} \\right| \\le 1
\\\\
\\begin{pmatrix}
\\underline{x_0} 
\\\\
x_1^*
\\end{pmatrix}
& : \\left| \\frac{w_1 - w_2}{w_2 a b \\pi} \\right| \\gt 1, \\: w_1 > w_2
\\\\
\\begin{pmatrix}
\\overline{x_0} 
\\\\
x_1^*
\\end{pmatrix}
& : \\left| \\frac{w_1 - w_2}{w_2 a b \\pi} \\right| \\gt 1, \\: w_1 < w_2
\\end{cases}
\\\\
k \\in \\mathcal{Z}
\\qquad 
x_1^* \\in \\mathcal{D}_1
\\qquad
\\left| x_1^* - 0 \\right| \\le \\left|x_1 - 0\\right| \\: \\forall \\, x_1 \\in \\mathcal{D}_1
\\end{gather}
\\quad .
```

**Arguments**\\
`x_extrema` Solution domain (``\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}``)\\
`a`, `b` Objectives parameters
"""
function generate_xopt(
	x_extrema::NTuple{4, Number},
	a::Number, 
	b::Number
)
	function x_opt(
		w1::Number, 
		w2::Number
	)
		acos_arg = (w1 - w2)/(w2*a*b*π)
		x0_opt = if abs(acos_arg) > 1 
			w1 > w2 ? x_extrema[1] : x_extrema[2]
		else  
			# Identify all local minima at least within the solution domain
			local_min_down = range(
				start = sign(a) * acos(acos_arg)/(b*π),
				step = - 2/b,
				stop = x_extrema[1])
			local_min_up = range(
				start = sign(a) * acos(acos_arg)/(b*π),
				step = 2/b,
				stop = x_extrema[2])
			# Ensure no minima is outside the solution domain
			all_local_min = filter(
				x -> (x >= x_extrema[1]) && (x <= x_extrema[2]),
				[local_min_down; local_min_up])
			# Keep the largest one if there is at least one ...
			if length(all_local_min) > 0
				maximum(all_local_min)
			# otherwise select either one of the two extrema
			else
				f2 = generate_f2(a, b)
				argmin(x -> f2([x, zero(x)]), [x_extrema[1], x_extrema[2]]) 
			end
		end
		x1_opt = if sign(x_extrema[3] * x_extrema[4]) == 1.0
			# Get the extrema that is the nerest to zero
			argmin(x -> abs(x - zero(x)), x_extrema[3:4]) 
		else
			zero(x_extrema[3])
		end		
		return [x0_opt, x1_opt]
	end
	
	return x_opt
end

# ╔═╡ 4c8e1882-3899-4bf0-a1ba-ed6caaea5944
"""
	xopt_c(w1::Number, w2::Number)

Convex instance of the weighted sum model optimal solution within the solution domain:

```math
\\mathbf{x}^*(w_1, w_2)
=
\\mathbf{x}^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, a, b, w_1, w_2)
\\Bigr|_{\\substack{
	\\underline{x_0} = $(opt_prob_spec.x0_min)\\\\
	\\overline{x_0} = $(opt_prob_spec.x0_max)\\\\
	\\underline{x_1} = $(opt_prob_spec.x1_min)\\\\
	\\overline{x_1} = $(opt_prob_spec.x1_max)\\\\
	a = $(f2_spec_convex.a)\\\\
	b = $(f2_spec_convex.b)
}}
```

**Arguments**\\
`w1`, `w2` Weighted sum model weights
"""
xopt_c = generate_xopt(
	getfield.(Ref(opt_prob_spec), (:x0_min, :x0_max, :x1_min, :x1_max)),
	getfield.(Ref(f2_spec_convex), (:a, :b))...)

# ╔═╡ d5707986-ebb4-4f53-b089-55cccfce05e4
"""
	xopt_nc(w1::Number, w2::Number)

Non-convex instance of the weighted sum model optimal solution within the solution domain:

```math
\\mathbf{x}^*(w_1, w_2)
=
\\mathbf{x}^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, a, b, w_1, w_2)
\\Bigr|_{\\substack{
	\\underline{x_0} = $(opt_prob_spec.x0_min)\\\\
	\\overline{x_0} = $(opt_prob_spec.x0_max)\\\\
	\\underline{x_1} = $(opt_prob_spec.x1_min)\\\\
	\\overline{x_1} = $(opt_prob_spec.x1_max)\\\\
	a = $(f2_spec_nonconvex.a)\\\\
	b = $(f2_spec_nonconvex.b)
}}
```

**Arguments**\\
`w1`, `w2` Weighted sum model weights
"""
xopt_nc = generate_xopt(
	getfield.(Ref(opt_prob_spec), (:x0_min, :x0_max, :x1_min, :x1_max)),
	getfield.(Ref(f2_spec_nonconvex), (:a, :b))...)

# ╔═╡ d7d09a77-284b-4896-85af-96e4f135e090
"""
	xopt_u(w1::Number, w2::Number)

User instance of the weighted sum model optimal solution within the solution domain:

```math
\\mathbf{x}^*(w_1, w_2)
=
\\mathbf{x}^*(\\underline{x_0}, \\overline{x_0}, \\underline{x_1}, \\overline{x_1}, a, b, w_1, w_2)
\\Bigr|_{\\substack{
	\\underline{x_0} = $(opt_prob_spec.x0_min)\\\\
	\\overline{x_0} = $(opt_prob_spec.x0_max)\\\\
	\\underline{x_1} = $(opt_prob_spec.x1_min)\\\\
	\\overline{x_1} = $(opt_prob_spec.x1_max)\\\\
	a = $(opt_prob_spec.a)\\\\
	b = $(opt_prob_spec.b)
}}
```

**Arguments**\\
`w1`, `w2` Weighted sum model weights
"""
xopt_u = generate_xopt(
	getfield.(Ref(opt_prob_spec), (:x0_min, :x0_max, :x1_min, :x1_max)),
	getfield.(Ref(opt_prob_spec), (:a, :b))...)

# ╔═╡ 89e81db4-3f67-4c24-8234-e84bea830e92
md"""
#### Weights domain
"""

# ╔═╡ 3a582ca2-a5e4-476f-ad95-a99cad3d23cf
md"""
Observe how for ``\left| \frac{w_1 - w_2}{w_2 a b \pi} \right| \gt 1`` the optimal solution does not dependent on the values of the weights. Under such condition there is only one optimal solution. On the contrary, distinct unique solutions can be found when

```math
-1 \le \frac{w_1 - w_2}{w_2 a b \pi} \le 1
\quad .
```
"""

# ╔═╡ dc0778e5-2f82-4795-91ec-688be49a5789
md"""
To solve the above inequalities ``w_1`` is considered a free variable. This is done exploiting the fact that ``0 \le w_1 \le 1`` and ``w_2 = 1 - w_1``. Then, the following shall apply:

```math
\begin{cases}
w_1 \left(1 - \frac{2}{a b \pi}\right) \le 1 - \frac{1}{a b \pi} \\
w_1 \left(1 + \frac{2}{a b \pi}\right) \le 1 + \frac{1}{a b \pi}
\end{cases}
\quad 
\Longrightarrow
\quad
\begin{cases}
\frac{1 + \frac{1}{a b \pi}}{1 + \frac{2}{a b \pi}} \le w_1  \le \frac{1 - \frac{1}{a b \pi}}{1 - \frac{2}{a b \pi}} & : \frac{2}{a b \pi} \lt -1 
\\
0 \le w_1 \le \min \left(\frac{1 - \frac{1}{a b \pi}}{1 - \frac{2}{a b \pi}}, \frac{1 + \frac{1}{a b \pi}}{1 + \frac{2}{a b \pi}}\right) & : -1 \lt \frac{2}{a b \pi} \lt 1 
\\
\frac{1 - \frac{1}{a b \pi}}{1 - \frac{2}{a b \pi}} \le w_1 \le \frac{1 + \frac{1}{a b \pi}}{1 + \frac{2}{a b \pi}} & : \frac{2}{a b \pi} \gt 1
\end{cases} 
\quad .
```
"""

# ╔═╡ 8c961af7-9e79-4c11-abe5-0734fadbaaaa
"""
    w1_extrema(a::Number, b::Number)

Returns the boundaries of weight ``w_1`` within which the weighted sum optimisation problem solves to unique distinct solutions:

```math
\\mathcal{D}_{w_1}(a, b) = [\\underline{w_1}(a, b), \\overline{w_1}(a, b)] \\subset \\mathcal{R} 
```

**Arguments**\\
`a`, `b` Objectives parameters
"""
function w1_extrema(
	a::Number,
	b::Number
)
	c0 = a * b * π
	cp = (1 + 1/c0) / (1 + 2/c0) 
	cn = (1 - 1/c0) / (1 - 2/c0) 

	if 2/c0 < -1
		w_min = cp
		w_max = cn
	elseif (2/c0 > -1) & (2/c0 < 1)
		w_min = -Inf
		w_max = min(cn, cp)
	elseif 2/c0 > 1
		w_min = cn
		w_max = cp
	else
		print("Error")
	end

	return [max(w_min, 0), min(w_max, 1)]
end

# ╔═╡ 4f548fc6-6b37-4059-80f1-060cd92cd911
begin
	Dw1_c = w1_extrema(f2_spec_convex.a, f2_spec_convex.b)
	Dw1_nc = w1_extrema(f2_spec_nonconvex.a, f2_spec_nonconvex.b)
	Dw1_u = w1_extrema(opt_prob_spec.a, opt_prob_spec.b)
	
	Markdown.parse("""
		The domain of weight ``w_1`` is computed for the convex, non-convex and user-defined cases. The domain of weight ``w_2`` can then be computed deriving from that of ``w_1``:
		
		```math
		\\begin{aligned}
		&\\frac{2}{ab\\pi} = &&$(2/(f2_spec_convex.a * f2_spec_convex.b * π))
		&&\\frac{2}{ab\\pi} = &&$(2/(f2_spec_nonconvex.a * f2_spec_nonconvex.b * π))
		&&\\frac{2}{ab\\pi} = &&$(2/(opt_prob_spec.a * opt_prob_spec.b * π))
		\\\\
		&\\mathcal{D}_{w_{1_\\text{c}}} = &&[$(Dw1_c[1]), $(Dw1_c[2])]
		&&\\mathcal{D}_{w_{1_\\text{nc}}} = &&[$(Dw1_nc[1]), $(Dw1_nc[2])]
		&&\\mathcal{D}_{w_{1_\\text{u}}} = &&[$(Dw1_u[1]), $(Dw1_u[2])]
		\\\\
		&\\mathcal{D}_{w_{2_\\text{c}}} = &&[$(1-Dw1_c[2]), $(1-Dw1_c[1])]
		&&\\mathcal{D}_{w_{2_\\text{nc}}} = &&[$(1-Dw1_nc[2]), $(1-Dw1_nc[1])]
		&&\\mathcal{D}_{w_{2_\\text{u}}} = &&[$(1-Dw1_u[2]), $(1-Dw1_u[1])]
		&&\\end{aligned}
		```
		""")
end

# ╔═╡ 3f57ad18-d19b-4c64-91df-2c9764f31a76
md"""
#### Result
"""

# ╔═╡ 0f90a21c-b2b8-487b-b712-87f41daf775a
md"""
The weighted sum model is the most straightforward method for scalarising multiple objectives. However, it is not without its limitations:
- Requires manual interventions for the specification of the weights;
- Even distribution of the weights generally lead to uneven distibution of the solution points;
- Does not provide all Pareto dominant points for non-convex problems 
"""

# ╔═╡ b9908a13-83e3-4160-a17b-eb00a6c6e338
let
	range_options = (length = analyses_spec.len, )
	w2(w1) = 1 - w1
	
	fig, axes = make_outcomespace_basefigure()		
	draw_random_outcomes(axes)
	
	centeraxes_kwargs = (xlabel = L"x_0^*", aspect = MK.DataAspect(), 
		limits = (nothing, nothing, -0.25, 0.25), )
	centeraxes = [
		MK.Axis(fig[2, 1]; ylabel = L"x_1^*", centeraxes_kwargs...),
		MK.Axis(fig[2, 2]; centeraxes_kwargs...),
		MK.Axis(fig[2, 3]; centeraxes_kwargs...)]
	MK.linkxaxes!(centeraxes...)
	MK.linkyaxes!(centeraxes...)
	
	bottomaxes_kwargs = (xlabel = L"w_1", aspect = MK.DataAspect(), )
	bottomaxes = [
		MK.Axis(fig[3, 1]; ylabel = L"w_2", bottomaxes_kwargs...), 
		MK.Axis(fig[3, 2]; bottomaxes_kwargs...), 
		MK.Axis(fig[3, 3]; bottomaxes_kwargs...)]
	MK.linkxaxes!(bottomaxes...)
	MK.linkyaxes!(bottomaxes...)
	
	# Left - Convex
	w1_range = [0; range(Dw1_c...; range_options...); 1]
	xopt = xopt_c.(w1_range, w2.(w1_range))

	axis = axes[1]
	MK.scatter!(axis, f1.(xopt), f2_c.(xopt), 
		color = w1_range; scatter_fopt_options...)
	
	axis = centeraxes[1] 
	MK.scatter!(axis, MK.Point2f.(xopt), 
		color = w1_range; scatter_fopt_options...)
		
	axis = bottomaxes[1] 
	MK.scatter!(axis, w1_range, w2.(w1_range), 
		color = w1_range; scatter_fopt_options...)

	# Center - Non-convex
	w1_range = [0; range(Dw1_nc...; range_options...); 1]
	xopt = xopt_nc.(w1_range, w2.(w1_range))

	axis = axes[2]
	MK.scatter!(axis, f1.(xopt), f2_nc.(xopt), 
		color = w1_range; scatter_fopt_options...)
	
	axis = centeraxes[2] 
	MK.scatter!(axis, MK.Point2f.(xopt), 
		color = w1_range; scatter_fopt_options...)
	
	axis = bottomaxes[2] 
	MK.scatter!(axis, w1_range, w2.(w1_range), 
		color = w1_range; scatter_fopt_options...)
	
	# Right - User-defined
	w1_range = [0; range(Dw1_u...; range_options...); 1]
	xopt = xopt_u.(w1_range, w2.(w1_range))

	axis = axes[3]
	MK.scatter!(axis, f1.(xopt), f2_u.(xopt), 
		color = w1_range; scatter_fopt_options...)

	axis = centeraxes[3] 
	MK.scatter!(axis, MK.Point2f.(xopt), 
		color = w1_range; scatter_fopt_options...)
	
	axis = bottomaxes[3] 
	MK.scatter!(axis, w1_range, w2.(w1_range), 
		color = w1_range; scatter_fopt_options...)
	
	# Top title axis
	MK.Label(fig[0, :], "Optimal points with the weighted sum model")

	# Enforce the correct aspect of second row
	MK.rowsize!(fig.layout, 2, MK.Aspect(1, 0.5))

	# Enforce the correct aspect of third row
	MK.rowsize!(fig.layout, 3, MK.Aspect(1, 1.0))
	
	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ d9f2d089-1d3b-419a-800d-42c4e6c96165
md"""
The geometrical interpretation of the method allows to better understand the underlying working principle. The weights can be thought of as defining a direction in the objective space. The optimal solutions of the scalarized problem correspond to points on the Pareto front that are at the interestion between the feasible region and the hyperplane normal to the weights-defined direction. As the weights change, the hyperplane rotates, leading to different intersection points along the Pareto front.
"""

# ╔═╡ d8490e44-4b6a-4273-996c-82a3aaf0e70f
let
	w1_slider = PUI.Slider(0:0.01:1.0, default = 0.3, show_value = true)
	
	md"""
	``w_1`` $(@bind w1_sample w1_slider)
	"""
end

# ╔═╡ 8b9f966b-7c40-477f-a584-e0269e06b0b8
let
	fig, axes = make_outcomespace_basefigure()
	draw_random_outcomes(axes)
	
	axis_options = (aspect = MK.DataAspect(), )
	ablines_weights_options = (color = :blue, label = "weight direction", 
		linestyle = (:dashdot, :loose), )
	ablines_hyper_options = (color = :teal, label = "hyperplane", )
	arrows_options = (arrowsize = 28, ablines_weights_options.color, )

	w2(w1) = 1 - w1
	weight_slope = w2(w1_sample)/w1_sample
	hyper_slope = -1/weight_slope
	hyper_intercept(x1_opt, x2_opt) = x2_opt - x1_opt*hyper_slope
	arrows_point(x1_opt, x2_opt) = MK.Point2f([
		hyper_intercept(x1_opt, x2_opt) / (weight_slope - hyper_slope),
		weight_slope*hyper_intercept(x1_opt, x2_opt) / (weight_slope - hyper_slope)])
	arrows_direction = MK.Vec2f([0.05, 0.05*weight_slope])

	# Left - Convex
	xopt = xopt_c(w1_sample, w2(w1_sample))

	axis = axes[1]
	MK.scatter!(axis, f1(xopt), f2_c(xopt); 
		scatter_fopt_options...)
	MK.ablines!(axis, 0.0, weight_slope;
		ablines_weights_options...)
	MK.ablines!(axis, hyper_intercept(f1(xopt), f2_c(xopt)), hyper_slope;
		ablines_hyper_options...)
	MK.arrows!(axis, [arrows_point(f1(xopt), f2_c(xopt))], [arrows_direction];
		arrows_options...)

	# Center - Non-convex
	xopt = xopt_nc(w1_sample, w2(w1_sample))

	axis = axes[2]
	MK.scatter!(axis, f1(xopt), f2_nc(xopt); 
		scatter_fopt_options...)
	MK.ablines!(axis, 0.0, weight_slope;
		ablines_weights_options...)
	MK.ablines!(axis, hyper_intercept(f1(xopt), f2_nc(xopt)), hyper_slope;
		ablines_hyper_options...)
	MK.arrows!(axis, [arrows_point(f1(xopt), f2_nc(xopt))], [arrows_direction];
		arrows_options...)

	# Legend in centre axis
	MK.axislegend(axis)

	# Right - User-defined
	xopt = xopt_u(w1_sample, w2(w1_sample))

	axis = axes[3]	
	MK.scatter!(axis, f1(xopt), f2_u(xopt); 
		scatter_fopt_options...)
	MK.ablines!(axis, 0.0, weight_slope;
		ablines_weights_options...)
	MK.ablines!(axis, hyper_intercept(f1(xopt), f2_u(xopt)), hyper_slope;
		ablines_hyper_options...)
	MK.arrows!(axis, [arrows_point(f1(xopt), f2_u(xopt))], [arrows_direction];
		arrows_options...)
	
	# Top title axis
	MK.Label(fig[0, :], "Optimal points with the weighted sum model")

	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ 07c80c8e-16ae-4467-b78b-5f03a503e125
md"""
### Weighted sum of powers model
"""

# ╔═╡ f0bf5b04-b2f1-417e-bf6d-5bcd05a2f694
md"""
The weigthed power sum model...

```math
f_s(\mathbf{x}) = \sum_j w_j f_j(\mathbf{x})^p \quad ,
```

where ``w_j`` are weights and ``p`` the power factor.
"""

# ╔═╡ 1bb4f4ff-6a25-48c2-9150-26c0e0393feb
md"""
### Weighted distance metric model
"""

# ╔═╡ 79ee9bdc-b9b8-42ab-a68b-1d36508dc558
md"""
The weigthed distance metric model was introduced in [Yu 1973 - A Class of Solutions for Group Decision Problems](https://doi.org/10.1287/mnsc.19.8.936):

```math
f_s(\mathbf{x}) =
\sqrt[p]{\sum_j w_j^p \lvert f_j(\mathbf{x}) - f_j^*\rvert^p} 
\quad ,
```

where ``w_j`` are weights and ``p`` the power factor.
"""

# ╔═╡ 9d79fb74-6990-4a54-8f1f-f3f2ad78f696
md"""
### Weighted Chebyshev model
"""

# ╔═╡ 271f6b83-2ddc-41dd-8acc-007e3b7e6012
md"""
The weighted Chebyshev model is based on the weighted distance metric model:

```math
f_s(\mathbf{x}) =
\max_j \: w_j \lvert f_j(\mathbf{x}) - f_j^*\rvert
\quad , 
```

where ``w_j`` are weights.
"""

# ╔═╡ ca4cb32b-89e3-4b4c-8c44-7890a1ebabd3
md"""
### Weighted exponential model
"""

# ╔═╡ 2c3d6771-17bb-495e-b0fe-8da51347a085
md"""

```math
f_s(\mathbf{x}) =
\sum_j \left(e^{p w_j}-1\right)e^{p f_j(\mathbf{x})}
\quad ,
```

where ``w_j`` are weights.
"""

# ╔═╡ f01f591d-eeb4-4d7e-bf38-e03c2e3f1025
md"""
### Weighted product model
"""

# ╔═╡ ccc749e3-d390-47ab-accd-1fd6660eb87f
md"""

```math
f_s(\mathbf{x}) =
\prod_j \lvert f_j(\mathbf{x}) \rvert^{w_j}
```
"""

# ╔═╡ 141b9b12-5ca7-47b5-b062-80ab6f0aa217
md"""
### ``\epsilon``-constraint model
"""

# ╔═╡ 45dba192-1d25-4eff-886a-0689c15e403f
md"""
The $\epsilon$-constraint method, introduced in [Haimes et al., 1971](https://doi.org/10.1109/TSMC.1971.4308298), approaches the multi-objective problem by taking into account one objective at a time. The other objectives are then converted into constraints by setting an upper bound for each of them (hence the name of the method). This translates in:

```math
\begin{aligned}
\min \quad & f_k (\mathbf{x}) \\
\textrm{s.t.} \quad & f_j (\mathrm{x}) \le \epsilon_j  \quad \forall \; j \setminus k \\
	& \mathrm{x} \in \mathcal{D}
\end{aligned}
```
"""

# ╔═╡ 494159ff-1344-4c28-8181-28aade51730f
md"""
Taking the optimisation problem considered here, if the second objective function is selected for the optimisation, the optimisation problem is transformed into:

```math
\begin{aligned}
\min_\mathbf{x} \quad & f_2 (\mathbf{x}) \\
\textrm{s.t.} \quad & f_1 (\mathrm{x}) \le \epsilon_1 \\
	& \mathrm{x} \in \mathcal{D}
\end{aligned}
```
"""

# ╔═╡ d034645b-ab80-47e9-9780-8927f9bb6b3b
"""
    generate_ϵc_f2_problem_solver(f1::Function, f2::Function, n::Integer)::Function

Return an instance of the function that solves the minimisation problem

```math
\\begin{aligned}
\\min_\\mathbf{x} \\quad & f_2 (\\mathbf{x}) \\\\
\\textrm{s.t.} \\quad & f_1 (\\mathrm{x}) \\le \\epsilon_1 \\\\
	& \\mathrm{x} \\in \\mathcal{D}
\\end{aligned}
```

Note how the used minimisation algorithm is a local optimiser. Therefore it may fail in identifying the global optimal solution (within ``\\mathcal{D}``) depending on the initial guess. For this reason argument `n` is provided to repeat the optimisation multiple times with different random initial guesses. The ``\\arg \\min`` is returned.

**Arguments**\\
`f1` objective function 1, converted into a constraint\\
`f2` objective function 2, subject of the scalar minimisation problem\\
`n` number of resolution tries
"""
function generate_ϵc_f2_problem_solver(
	f1::Function,
	f2::Function,
	n::Integer = 5
)::Function
	# Specify the optimisation function with differentiation method and constraints
	f_obj(x, _p) = f2(x)
	f_con(res, x, _p) = (res .= [f1(x)])
	opt_fun = OPT.OptimizationFunction(
		f_obj, OPT.AutoFiniteDiff(),
		cons = f_con)

	function solve_ϵc_f2_problem(	
		ϵ1::Number
	)::AbstractVector
		# Specify multiple initial guesses since the solver is a local optimiser
		x0s = rand(filter(x -> (f1(x) <= ϵ1), x), n)

		# Specify solution bounds
		lb = getfield.(Ref(opt_prob_spec), [:x0_min, :x1_min])
		ub = getfield.(Ref(opt_prob_spec), [:x0_max, :x1_max])

		opt_sols = map(x0s) do x0
			# Specify the optimisation problem with bounds 
			opt_prob = OPT.OptimizationProblem(
				opt_fun, x0,
				lb = lb, ub = ub,
				lcons = [-Inf], ucons = [ϵ1])
	
			# Compute the solution with the specified solver
			OPT.solve(opt_prob, OPTJL.IPNewton()).u
		end
			
		return sort(opt_sols, by = f2) |> first
	end

	return solve_ϵc_f2_problem
end

# ╔═╡ 5f4e2fc7-900e-4ae3-9ced-8baa34a7f419
"""
    solve_ϵc_f2c_problem(ϵ1::Number)::AbstractVector

Solve the ``\\epsilon``-constraint problem associated with the optimisation problem with convex ``f_2``:


```math
\\begin{aligned}
\\min_\\mathbf{x} \\quad & f_{2_\\text{c}} (\\mathbf{x}) \\\\
\\textrm{s.t.} \\quad & f_1 (\\mathrm{x}) \\le \\epsilon_1 \\\\
	& \\mathrm{x} \\in \\mathcal{D}
\\end{aligned}
```

**Arguments**\\
`ϵ1` Constraint on function 1 
"""
solve_ϵc_f2c_problem = generate_ϵc_f2_problem_solver(f1, f2_c)

# ╔═╡ 24d391b0-8874-4414-ba44-59e0b0dc072e
"""
    solve_ϵc_f2nc_problem(ϵ1::Number)::AbstractVector

Solve the ``\\epsilon``-constraint problem associated with the optimisation problem with non-convex ``f_2``:

```math
\\begin{aligned}
\\min_\\mathbf{x} \\quad & f_{2_\\text{nc}} (\\mathbf{x}) \\\\
\\textrm{s.t.} \\quad & f_1 (\\mathrm{x}) \\le \\epsilon_1 \\\\
	& \\mathrm{x} \\in \\mathcal{D}
\\end{aligned}
```

**Arguments**\\
`ϵ1` Constraint on function 1 
"""
solve_ϵc_f2nc_problem = generate_ϵc_f2_problem_solver(f1, f2_nc)

# ╔═╡ 508a3f60-62ec-49ca-b571-88aa1437341e
"""
    solve_ϵc_f2u_problem(ϵ1::Number)::AbstractVector

Solve the ``\\epsilon``-constraint problem associated with the optimisation problem with user-defined ``f_2``:

```math
\\begin{aligned}
\\min_\\mathbf{x} \\quad & f_{2_\\text{u}} (\\mathbf{x}) \\\\
\\textrm{s.t.} \\quad & f_1 (\\mathrm{x}) \\le \\epsilon_1 \\\\
	& \\mathrm{x} \\in \\mathcal{D}
\\end{aligned}
```

**Arguments**\\
`ϵ1` Constraint on function 1 
"""
solve_ϵc_f2u_problem = generate_ϵc_f2_problem_solver(f1, f2_u)


# ╔═╡ 5992c90e-e707-48ca-b633-1f48715c8606
let
	f1_extrema = extrema(f1.(x))
	ϵ1_range = range(round.(f1_extrema .+ [1, -1].*1e-2, digits = 2)..., 
		step = 1e-2)
	ϵ1_slider = PUI.Slider(ϵ1_range, default = ϵ1_range[end], show_value = true)
	
	md"""
	``\epsilon_1`` $(@bind ϵ1_sample ϵ1_slider)
	"""
end

# ╔═╡ c02c6696-64ce-4106-86db-64c49480fb53
let
	fig, axes = make_outcomespace_basefigure()
	draw_random_outcomes(axes)
		
	# Left - Convex
	axis = axes[1]

	x_opt = solve_ϵc_f2c_problem(ϵ1_sample)
	MK.scatter!(axis, f1(x_opt), f2_c(x_opt); 
		scatter_fopt_options..., color = :red)
	MK.vlines!(axis, ϵ1_sample;
		vlines_options..., color = :red)
	
	# Center - Non-convex
	axis = axes[2]
	
	x_opt = solve_ϵc_f2nc_problem(ϵ1_sample)
	MK.scatter!(axis, f1(x_opt), f2_nc(x_opt); 
		scatter_fopt_options..., color = :red)
	MK.vlines!(axis, ϵ1_sample;
		vlines_options..., color = :red)

	# Center - User-defined
	axis = axes[3]

	x_opt = solve_ϵc_f2u_problem(ϵ1_sample)
	MK.scatter!(axis, f1(x_opt), f2_u(x_opt); 
		scatter_fopt_options..., color = :red)
	MK.vlines!(axis, ϵ1_sample;
		vlines_options..., color = :red)

	# Title axis
	MK.Label(fig[0, :], "Weakly Pareto optimal solution")
	
	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ db2ba59a-d28d-46ce-b13c-8be479e57e39
md"""
Noteworthy results (actually theorems) are:

1. The solution $\mathrm{x}^\star$ of $\epsilon$-constraint problem is weakly Pareto optimal;

2. A solution $\mathrm{x}^\star$ is Pareto optimal if and only if it is a solution of the $\epsilon$-constraint problem for **all** $k$ and:

```math
\epsilon_j = f_j(\mathrm{x}^\star) \quad \forall \; j \setminus k  
\quad ;
```

3. A solution $\mathrm{x}^\star$ is Pareto optimal if and only if it is a **unique** solution of the $\epsilon$-constraint problem for **some** $k$ and correspondingly:

```math
\epsilon_j = f_j(\mathrm{x}^\star) \quad \forall \; j \setminus k  
\quad .
```
"""

# ╔═╡ 411bebc8-af0d-412f-a973-643d9574dac7
let
	ϵ1_range = range(
		round.(extrema(f1.(x)) .+ [1, -1].*1e-2, digits = 2)..., 
		step = 1e-2)

	fig, axes = make_outcomespace_basefigure()
	draw_random_outcomes(axes)

	centeraxes_kwargs = (xlabel = L"x_0^*", aspect = MK.DataAspect(), 
		limits = (nothing, nothing, -0.25, 0.25), )
	centeraxes = [
		MK.Axis(fig[2, 1]; ylabel = L"x_1^*", centeraxes_kwargs...),
		MK.Axis(fig[2, 2]; centeraxes_kwargs...),
		MK.Axis(fig[2, 3]; centeraxes_kwargs...)]
	MK.linkxaxes!(centeraxes...)
	MK.linkyaxes!(centeraxes...)
	
	# Convex
	x_opt = solve_ϵc_f2c_problem.(ϵ1_range)
	
	axis = axes[1]
	MK.scatter!(axis, f1.(x_opt), f2_c.(x_opt); 
		scatter_fopt_options..., color = ϵ1_range)

	axis = centeraxes[1]
	MK.scatter!(axis, MK.Point2f.(x_opt), 
		color = ϵ1_range; scatter_fopt_options...)
	
	# Non-convex
	x_opt = solve_ϵc_f2nc_problem.(ϵ1_range)

	axis = axes[2]
	MK.scatter!(axis, f1.(x_opt), f2_nc.(x_opt); 
		scatter_fopt_options..., color = ϵ1_range)

	axis = centeraxes[2]
	MK.scatter!(axis, MK.Point2f.(x_opt), 
		color = ϵ1_range; scatter_fopt_options...)
	
	# User-defined
	x_opt = solve_ϵc_f2u_problem.(ϵ1_range)

	axis = axes[3]
	MK.scatter!(axis, f1.(x_opt), f2_u.(x_opt); 
		scatter_fopt_options..., color = ϵ1_range)

	axis = centeraxes[3]
	MK.scatter!(axis, MK.Point2f.(x_opt), 
		color = ϵ1_range; scatter_fopt_options...)
	
	# Horizontal colorbars
	MK.Colorbar(fig[3, :], 
		limits = extrema(ϵ1_range), colormap = :winter, vertical = false,
		label = L"\epsilon_1", flipaxis = false)
	
	# Title
	MK.Label(fig[0, :], "All ϵ-constraint solutions are weakly Pareto optimal solutions")

	# Enforce the correct aspect of second row
	MK.rowsize!(fig.layout, 2, MK.Aspect(1, 0.5))
	
	MK.resize_to_layout!(fig)
	fig
end

# ╔═╡ 05a5dacb-9e71-42e2-b19c-532b430b9b89
md"""
Once above theorem 1 is used to identify the larger set of weakly Pareto optimal solutions, either theorem 2 or 3 is usually applied to identify the Pareto optimal subset. Which of the two to use dependes on the optimisation problem:

* When it is possible to assess the uniqueness of a solution, approach 3 is the easier one. This is the case for convex problems, for which a solution to the ϵ-constraint problem is usually unique. Testing then for the equality of constratins is not computationally intensive; 

* For non-convex problems the uniqueness of a solution to the ϵ-constratint problem is not guaranted. Approach 2, that does not require the solution uniqueness, shall be followed. However the number of optimisation problems to be solved increases considerably.
"""

# ╔═╡ Cell order:
# ╟─cbd87dfa-4d86-11ef-2960-4123987c10c2
# ╟─733b2573-753a-453d-be7e-59dfcebcae98
# ╟─638c883b-c7f6-430c-bcc5-bf05930fab17
# ╟─7adcb646-f43c-40df-84d1-7a9cad10f181
# ╟─180910cb-ed20-4912-993e-d3fe19372583
# ╟─856d274c-c6fa-4e97-86d3-33fc3a913182
# ╟─d3fbffba-6ed0-4bf8-a314-c9da1506505b
# ╟─efdd67dd-45f7-4780-a87a-eade5bd4e6b6
# ╟─617fb846-3cf0-4af6-aafd-087721b56a9f
# ╟─e95e4249-605a-4ce1-b1d3-e9100a393ff4
# ╟─22d2a3e9-6745-4e33-88fb-55f7a5fc8ea5
# ╟─4cf8f36d-5899-4600-af28-569fe879a395
# ╟─6a491ff3-8728-43bb-84c4-7e6f0a72bcbe
# ╟─d12ef486-2bda-4abf-ba75-f063d4feafba
# ╟─802d187a-f75a-4d5f-b1ab-175a7635846e
# ╟─86a6579e-4a95-4be6-8ff9-335b746353e2
# ╟─b37c699c-0c6c-479a-969b-c1064c82fa3d
# ╟─887d96fb-7655-4b82-8095-e93b90d33846
# ╟─cbf25634-a447-41b3-91e6-d5d7369e1788
# ╟─b92bccea-c413-4045-9a32-2cfceda4bc89
# ╟─edbee35a-9603-4da8-b6e4-65c61e0e721d
# ╟─75b7a736-c837-4e17-b3e9-022768a6acca
# ╟─bd70fd5d-3191-48ca-a563-106111873153
# ╟─55504181-293c-4869-8e15-f4e35630a0d3
# ╟─24365b84-a680-4e7e-ad9b-2caff8158948
# ╟─4cce56cd-6ebc-4b27-b6a9-c5adaf1c6fef
# ╟─1f938bea-811b-4f79-8743-5f07bb62973a
# ╟─0bcddce6-92d5-4401-9ab6-65c0fcecac52
# ╟─308e84e5-7faf-422f-99fe-ef2f01b92c9d
# ╟─fec802f6-fe8f-4c44-8874-4c91bcb65b03
# ╟─9b09a8c7-896f-4910-8c78-06f1f8bb195d
# ╟─a94a30f7-c518-4836-9610-0b01d80b89ef
# ╟─1897e944-ee51-4646-9904-681252dc63fb
# ╟─cc1fd63d-2af4-4157-8dfc-7a51ee33b354
# ╟─5cd00774-458f-4c14-9d11-568f6d580479
# ╟─7dbff727-c994-407b-8b40-fc74486c8d89
# ╟─c412998e-456f-48c7-8722-7fd65f31fe8a
# ╟─deca5a7d-65f6-4be2-a35f-f7cb04264e57
# ╟─1d507ad2-5641-4357-9931-c55da7ce3212
# ╟─d341743d-0333-41a7-b545-a340e697e8ce
# ╟─4c8e1882-3899-4bf0-a1ba-ed6caaea5944
# ╟─d5707986-ebb4-4f53-b089-55cccfce05e4
# ╟─d7d09a77-284b-4896-85af-96e4f135e090
# ╟─89e81db4-3f67-4c24-8234-e84bea830e92
# ╟─3a582ca2-a5e4-476f-ad95-a99cad3d23cf
# ╟─dc0778e5-2f82-4795-91ec-688be49a5789
# ╟─8c961af7-9e79-4c11-abe5-0734fadbaaaa
# ╟─4f548fc6-6b37-4059-80f1-060cd92cd911
# ╟─3f57ad18-d19b-4c64-91df-2c9764f31a76
# ╟─0f90a21c-b2b8-487b-b712-87f41daf775a
# ╟─b9908a13-83e3-4160-a17b-eb00a6c6e338
# ╟─d9f2d089-1d3b-419a-800d-42c4e6c96165
# ╟─d8490e44-4b6a-4273-996c-82a3aaf0e70f
# ╟─8b9f966b-7c40-477f-a584-e0269e06b0b8
# ╟─07c80c8e-16ae-4467-b78b-5f03a503e125
# ╟─f0bf5b04-b2f1-417e-bf6d-5bcd05a2f694
# ╟─1bb4f4ff-6a25-48c2-9150-26c0e0393feb
# ╟─79ee9bdc-b9b8-42ab-a68b-1d36508dc558
# ╟─9d79fb74-6990-4a54-8f1f-f3f2ad78f696
# ╟─271f6b83-2ddc-41dd-8acc-007e3b7e6012
# ╟─ca4cb32b-89e3-4b4c-8c44-7890a1ebabd3
# ╟─2c3d6771-17bb-495e-b0fe-8da51347a085
# ╟─f01f591d-eeb4-4d7e-bf38-e03c2e3f1025
# ╟─ccc749e3-d390-47ab-accd-1fd6660eb87f
# ╟─141b9b12-5ca7-47b5-b062-80ab6f0aa217
# ╟─45dba192-1d25-4eff-886a-0689c15e403f
# ╟─494159ff-1344-4c28-8181-28aade51730f
# ╟─d034645b-ab80-47e9-9780-8927f9bb6b3b
# ╟─5f4e2fc7-900e-4ae3-9ced-8baa34a7f419
# ╟─24d391b0-8874-4414-ba44-59e0b0dc072e
# ╟─508a3f60-62ec-49ca-b571-88aa1437341e
# ╟─5992c90e-e707-48ca-b633-1f48715c8606
# ╟─c02c6696-64ce-4106-86db-64c49480fb53
# ╟─db2ba59a-d28d-46ce-b13c-8be479e57e39
# ╟─411bebc8-af0d-412f-a973-643d9574dac7
# ╟─05a5dacb-9e71-42e2-b19c-532b430b9b89
