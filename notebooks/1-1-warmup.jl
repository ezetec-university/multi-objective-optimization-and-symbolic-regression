### A Pluto.jl notebook ###
# v0.19.45

#> [frontmatter]
#> title = "01.1 Warmup"
#> date = "2024-07-12"
#> 
#>     [[frontmatter.author]]
#>     name = "Emanuele Zarfati"

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 373c9ce4-407a-11ef-06a2-a5009e299142
begin
    import Pkg
    # activate the shared project environment
    Pkg.activate(Base.current_project())
    # instantiate, i.e. make sure that all packages are downloaded
    Pkg.instantiate()
end;

# ╔═╡ 46d09102-58ca-478b-9147-bbdb1570df50
begin
	# Dynamic inputs
	import PlutoUI as PUI
	import PlutoUIExtra as PUIE
	
	# Maths
	import Distributions as DST
	import Optimization as OPT
	import OptimizationOptimJL as OPTJL
	
	# Graphics (frontend e backend)
	import Makie as MK
	import CairoMakie as CMK
	using LaTeXStrings
	
	md"""
	Dependences loaded.
	"""
end

# ╔═╡ b3fbe5ca-428d-4140-9630-b9c621645819
md"""
# 1.1 Warm-up
"""

# ╔═╡ c8f6f90a-0ec6-483c-a8a7-2a562776760e
md"""
## Notebook set-up
"""

# ╔═╡ 2abebd4d-2afc-4387-b18b-2f86c6d40c05
begin 
	CMK.activate!()
	MK.set_theme!(
		MK.Theme(
			size = (1600, 600),
			fonts=(; 
				regular = MK.texfont(:text),
				bold = MK.texfont(:bold),
				italic = MK.texfont(:italic),
				bold_italic = MK.texfont(:bolditalic)),
			font = :regular,
			fontsize = 38,
			Axis = (
				rightspinevisible = false,
				topspinevisible = false,
				xgridvisible = false,
				ygridcolor = :grey90,
				xlabelsize = 38,
				ylabelsize = 38,
				labelsize = 38,
				titlefont = :regular
			)
		)
	)
	md"""
	Configurations applied.
	"""
end

# ╔═╡ e41b388d-ba2e-4f32-a940-172c76dc18a5
let
	toc_ui = PUI.TableOfContents(depth = 4)
	md"""
	Table of contents added.
	$(toc_ui)
	"""
end

# ╔═╡ 35f5f7d6-0bfc-4d6a-90dd-f1ba59be57ce
md"""
## Optimisation problem set-up
"""

# ╔═╡ 7c96bc9e-2618-4381-b88b-dbf5c62a409e
md"""
Define the objective functions to be minimised
"""

# ╔═╡ f6c2c882-78c3-4f20-be9a-8a316e54bdab
"""
    f1(x::Number)

Returns the value of the objective function 1
```math
f_1(x) = 0.2(x-1)^4 - 1
```
"""
function f1(
	x::Number
)
	return 0.2(x - 1)^4 + 1
end 

# ╔═╡ 4822d128-33ad-454f-b6af-66f8ba34f81f
"""
    f2(x::Number)

Returns the value of the objective function 2

```math
f_2(x) = 2.0(x - 3)^2 + 1
```
"""
function f2(
	x::Number
)
	return 2.0(x - 3)^2 + 1
end 

# ╔═╡ 29f041f3-e0f2-40b8-90d7-52c14c27b130
md"""
The objective functions are collected in a tuple:
"""

# ╔═╡ 9c2132de-10c7-4585-96b9-c0cfe55393d7
fi = (f1, f2)

# ╔═╡ 8c0b7594-286b-441f-9261-a35cb225789e
let
	# Definition of the independente variable
	x_combined_ui = PUI.combine() do Child	
		x_min_ui = Child("min", 
			PUI.Slider(-5.0:0.1:5.0, default = -2.0, show_value = true))
		x_Δ_ui = Child("spa", 
			PUI.Slider(0.0:0.1:10.0, default = 6.0, show_value = true))
		x_len_ui = Child("len", 
			PUI.Slider(10:1000, default = 50, show_value = true))

		md"""
		**Independent variable**\
		Generate random values of $x$:\
		Minimum: \
		$(x_min_ui) \
		Δ (max - min) \
		$(x_Δ_ui) \
		N. of points \
		$(x_len_ui) 
		"""
	end

	# Definition of the weights
	# Supports an arbitrary number of objective functions, even if this 
	# notebook can demonstrate only two.
	w_combined_ui = PUI.combine() do Child
		w_uis = [
			md""" $(
				Child(idx, PUI.NumberField(1.0:100.0, 1.0))
			) $idx
			"""  
			for idx ∈ "w" .* string.(1:length(fi))
		]
		
		md""" \
		**Weighted sum method**\
		Relative weight of the\
		objective functions:
		$(w_uis)
		"""
	end

	# Show the sidebar
	sidebar_ui = PUIE.Sidebar(
		(@bind x_spec PUI.confirm(x_combined_ui, label = "Apply")),
		(@bind w_spec PUI.confirm(w_combined_ui, label = "Apply"));
		location = "lower right"
	)
	md"""
	Sidebar with dynamic configurations added.
	$(sidebar_ui)
	"""
end

# ╔═╡ baf565a8-7e5b-4f8a-af9c-ba9425c4e12d
md"""
Randomly generate the independent variable values:
"""

# ╔═╡ cfbe39f3-06ea-4034-a791-e2161e6829f4
x = sort(
	[rand(DST.Uniform(x_spec.min, x_spec.min + x_spec.spa), x_spec.len);
	 ])

# ╔═╡ f10f58e1-c0ff-45d2-8c0d-b8beaf9e314c
let
	fig = MK.Figure()
	axis = MK.Axis(fig[1, 1],
		title = "Objective functions",
		xlabel = L"x",
		ylabel = L"f_j(x)")

	MK.lines!(axis, x, f1.(x),
			  label = L"f_1(x)")
	MK.lines!(axis, x, f2.(x),
			  label = L"f_2(x)")

	MK.axislegend(axis, 
				  position = :ct, orientation = :horizontal)

	fig
end

# ╔═╡ e4797177-c203-42c0-98e3-8fc4ecc9a050
let
	fig = MK.Figure()
	axis = MK.Axis(fig[1, 1],
		title = "Sample outcomes in the objective space",
		xlabel = L"f_1(x)",
		ylabel = L"f_2(x)")

	x_samples = rand(x, 10)
	tooltips = "x = " .* string.(round.(x_samples, digits = 1))

	MK.scatter!(axis, f1.(x_samples), f2.(x_samples))
	MK.lines!(axis, f1.(x), f2.(x), 
			  color = :tomato, linestyle = :dash)
	MK.tooltip!.([axis], f1.(x_samples), f2.(x_samples), tooltips,
		fontsize = 24)
	
	fig
end

# ╔═╡ bc1fcfa6-d328-494b-8839-f0a137aa8f26
md"""
## Defining dominance relation between points 
"""

# ╔═╡ f9d49aa3-5634-4e44-8339-79c5a8aec88c
md"""
Define the functions to assess outcomes and solutions dominance
"""

# ╔═╡ e9887e18-6187-4b05-bca4-fc56701f6ebd
begin
	"""
	    dominance(y1, y2)
	
	Test for Paredo dominance of outcome ``y_1`` over outcome ``y_2`` in a minimisation problem.

	```math
	y_1 \\preceq y_2 
		\\quad \\cap \\quad 
	\\exists \\, j \\mid y_{1_j} \\prec y_{2_j}
	```
	"""
	function dominance(y1, y2)
		return all(y1 .≤ y2) & any(y1 .< y2)
	end

	"""
	    dominance(x1, x2, fi::NTuple{M, Function} where M)
	
	Test for Paredo dominance of solution ``x_1`` over solution ``x_2`` in a minimisation problem with objective functions ``f_i``.
	
	```math
	f_k(x_1) \\preceq f_k(x_2) \\: \\forall k
		\\quad \\cap \\quad 
	\\exists \\, j \\mid f_j(x_1) \\prec f_j(x_2)
	```
	"""
	function dominance(x1, x2, fi::NTuple{M, Function} where M)
	
		# Outcomes in the objective space
		y1 = map.(fi, Ref(x1))
		y2 = map.(fi, Ref(x2))
	
		return dominance(y1, y2)
	end
end

# ╔═╡ f77541e6-7d5f-47c8-8c5a-bb3b05ae3803
let
	combined_ui = PUI.combine() do Child
		x1_ui = Child(
			"x1", 
			PUI.Slider(x_spec.min:0.1:x_spec.min+x_spec.spa,
				default = round(x_spec.min+x_spec.spa/3, digits=2), 
				show_value = true))
		x2_ui = Child(
			"x2", 
			PUI.Slider(x_spec.min:0.1:x_spec.min+x_spec.spa,
				default = round(x_spec.min+2x_spec.spa/3, digits=2), 
				show_value = true))
		
		md"""
		Test the relative dominance of two possible solutions:\
		\
		``x_1`` $(x1_ui) \
		``x_2`` $(x2_ui) 
		"""
	end

	@bind x_test combined_ui
end

# ╔═╡ 17acc188-b295-4b58-a951-7e001e7bc278
let
	fig = MK.Figure()

	x_samples = [x_test.x1, x_test.x2]
	tooltips = "(" .* 
		string.(round.(f1.(x_samples), digits = 1)) .* ", " .*
		string.(round.(f2.(x_samples), digits = 1)) .* ")"

	# Outcomes dominance
	axis = MK.Axis(fig[1, 1],
		title = L"Pareto dominance among the two solutions $x_1$ and $x_2$",
		xlabel = L"f_1(x)",
		ylabel = L"f_2(x)")

	map(enumerate(x_samples), [:blue, :purple]) do (idx, sample), color
		MK.scatter!(axis, f1(sample), f2(sample))

		other_sample = x_samples[
			setdiff(begin:end, findfirst(==(sample), x_samples))]
		dominates =  LaTeXString("x_$(string(idx))=$(sample) " .* (
			dominance(sample, other_sample[1], fi) ? 
				"dominates" : "does not dominate"))
		MK.band!(axis, [f1(sample), maximum(f1.(x))], f2(sample), maximum(f2.(x)),
			color = (color, 0.1), label = dominates)
	end
	
	MK.lines!(axis, f1.(x), f2.(x), 
			  color = :tomato, linestyle = :dash)
	MK.tooltip!.([axis], f1.(x_samples), f2.(x_samples), tooltips,
				 fontsize = 28)
	MK.axislegend(axis)
	
	fig
end

# ╔═╡ 331b7442-18a4-4d41-81e3-73f289a654d0
md"""
## Scalarisation of the objectives
"""

# ╔═╡ 87b6e1a6-84f6-453c-834f-46181bb71434
md"""
The multi-dimensional objective space laks of total ordering, making hard the comparisons among problem solutions. To overcome this a possible and trivial approach consists of transforming the multi-dimensioal outcomes into a scalar quantity through linear weighting. This way the minimisation problem becomes:

```math
\min_x \sum_{i=1}^M w_i f_i(x)
```
"""

# ╔═╡ e318f2fa-6da7-40a1-b7f9-470314039cc1
"""
    generate_scalar_linear_obj(fi::NTuple{M, Function} where M, wi)

Returns a function ``f_s(x)`` that computes the weighted sum of the objective functions ``\\sum_i w_i f_i`` at given solution ``x``.
"""
function generate_scalar_linear_obj(fi::NTuple{M, Function} where M, wi)
	scalar_linear_obj(x) = sum(map.(fi, Ref(x)) .* wi)
	return scalar_linear_obj
end

# ╔═╡ f00f4967-4579-45ee-b4e5-32cb30757e0a
begin
	w = [getfield(w_spec, Symbol(field)) 
		for field in "w" .* string.(1:length(fi))]
	w = w / sum(w, init = 0.0)
	
	Markdown.parse(
		"""
		The weigths provided as input (``\\rightarrow``) are scaled 
		to sum up to 1:\\
		""" *
		join(["``w_" * string(idx) * "``: " * string(val) 
			 for (idx,val) ∈ enumerate(w)], """
		\\
		"""))
end

# ╔═╡ 04762786-8e07-4f0a-bd5a-83fdb534cb15
let
	fig = MK.Figure(size = MK.get_attribute(MK.Scene().theme, :size).*(1,2))

	# Optimisation problem definition 
	scalarised_fi = generate_scalar_linear_obj(fi, w)
	f_obj(x, _p) = scalarised_fi(x)
	opt_fun = OPT.OptimizationFunction(f_obj, OPT.AutoFiniteDiff())
	opt_prob = OPT.OptimizationProblem(
		opt_fun, [((x -> x/2) ∘ sum ∘ collect ∘ extrema)(x)], 
		lb = [minimum(x)],
		ub = [maximum(x)])

	# Solve the optimisation problem
	x_opt = OPT.solve(opt_prob, OPTJL.BFGS()).u[1]
	
	# Top axis with the objective functions
	tooltip = "(" * string(round(scalarised_fi(x_opt), digits = 1)) * ")"

	axis = MK.Axis(fig[1, 1],
		title = L"Weighted sum model $f_s$ along with the objective functions",
		xlabel = L"x",
		ylabel = L"f_j(x)")

	map(enumerate(fi)) do (idx, f)
		MK.lines!(axis, x, f.(x),
			label = L"f_%$(idx)(x)")
	end
	MK.lines!(axis, x, scalarised_fi.(x),
			  label = L"f_s(x) = \sum w_i f_i")
	MK.scatter!(axis, x_opt, scalarised_fi(x_opt), 
		color = :red, label = "Pareto Optimal")
	MK.tooltip!(axis, x_opt, scalarised_fi(x_opt), tooltip,
		fontsize = 28)	
	MK.axislegend(axis, 
		position = :ct, 
		orientation = :horizontal)

	# Bottom axis with the specific solution
	tooltip = "(" * 
		string(round(f1(x_opt), digits = 1)) * ", " *
		string(round(f2(x_opt), digits = 1)) * ")"

	axis = MK.Axis(fig[2, 1],
		xlabel = L"f_1(x)",
		ylabel = L"f_2(x)")

	MK.lines!(axis, f1.(x), f2.(x), 
		color = :tomato, linestyle = :dash, 
		label = "Objective space solutions")
	MK.scatter!(axis, f1(x_opt), f2(x_opt), 
		color = :red, label = "Pareto Optimal")
	MK.tooltip!(axis, f1(x_opt), f2(x_opt), tooltip,
		fontsize = 28)	
	MK.axislegend(axis,
		position = :lt)
	
	fig
end

# ╔═╡ 5543aa65-af9d-41f5-82cd-168c87a02450
md"""
### Pareto Optimal points
"""

# ╔═╡ 8c5d3217-e8cf-421a-9773-c389dd257a93
md"""
Solve the minimization problem for all possible combinations of weights to identify the Pareto Optimal points. These form the Pareto Front.
"""

# ╔═╡ e6582c10-28e5-4179-8021-2455ef8c58b4
begin
	fig = MK.Figure(size = MK.get_attribute(MK.Scene().theme, :size).*(1,2))

	# Weights domain
	w1_values = 0.0:1e-2:1.0
	w2(w1) = 1.0 - w1

	# Search for the optimal points
	 sol = map(w1_values) do w1
		scalarised_fi = generate_scalar_linear_obj(fi, (w1, w2(w1)))
		f_obj(x, _p) = scalarised_fi(x)

		# Solve the optimisation problem
		opt_fun = OPT.OptimizationFunction(f_obj, OPT.AutoFiniteDiff())
		opt_prob = OPT.OptimizationProblem(
			opt_fun, [((x -> x/2) ∘ sum ∘ collect ∘ extrema)(x)], 
			lb = [minimum(x)],
			ub = [maximum(x)])
		opt_sol = OPT.solve(opt_prob, OPTJL.BFGS())
		
		(opt_sol.u[1], scalarised_fi(opt_sol.u[1]))
	end 
	x_opt, f_opt = (s -> (getindex.(s, 1), getindex.(s, 2)))(sol)

	# Top graph with the solutions
	axis = MK.Axis(fig[1, 1],
		xlabel = L"w_1 = 1 - w_2")
	
	MK.lines!(axis, w1_values, f_opt, 
		color = :tomato, linestyle = :dash, 
		label = "Objective function solution")
	MK.scatter!(axis, w1_values, x_opt, 
		color = :red, label = "Pareto Optimal")
	MK.axislegend(axis)
	
	# Bottom graph with the objectives space
	axis = MK.Axis(fig[2, 1],
		title = "Solutions of the weighted sum model",
		xlabel = L"f_1(x)",
		ylabel = L"f_2(x)")

	MK.lines!(axis, f1.(x), f2.(x), 
		color = :tomato, linestyle = :dash, 
		label = "Objective space solutions")
	MK.scatter!(axis, f1.(x_opt), f2.(x_opt), 
		color = :red, label = "Pareto Optimal")
	MK.axislegend(axis, 
		position = :lt)

	fig
end

# ╔═╡ Cell order:
# ╟─b3fbe5ca-428d-4140-9630-b9c621645819
# ╟─373c9ce4-407a-11ef-06a2-a5009e299142
# ╟─c8f6f90a-0ec6-483c-a8a7-2a562776760e
# ╟─46d09102-58ca-478b-9147-bbdb1570df50
# ╟─2abebd4d-2afc-4387-b18b-2f86c6d40c05
# ╟─e41b388d-ba2e-4f32-a940-172c76dc18a5
# ╟─8c0b7594-286b-441f-9261-a35cb225789e
# ╟─35f5f7d6-0bfc-4d6a-90dd-f1ba59be57ce
# ╟─7c96bc9e-2618-4381-b88b-dbf5c62a409e
# ╟─f6c2c882-78c3-4f20-be9a-8a316e54bdab
# ╟─4822d128-33ad-454f-b6af-66f8ba34f81f
# ╟─29f041f3-e0f2-40b8-90d7-52c14c27b130
# ╟─9c2132de-10c7-4585-96b9-c0cfe55393d7
# ╟─baf565a8-7e5b-4f8a-af9c-ba9425c4e12d
# ╟─cfbe39f3-06ea-4034-a791-e2161e6829f4
# ╟─f10f58e1-c0ff-45d2-8c0d-b8beaf9e314c
# ╟─e4797177-c203-42c0-98e3-8fc4ecc9a050
# ╟─bc1fcfa6-d328-494b-8839-f0a137aa8f26
# ╟─f9d49aa3-5634-4e44-8339-79c5a8aec88c
# ╟─e9887e18-6187-4b05-bca4-fc56701f6ebd
# ╟─f77541e6-7d5f-47c8-8c5a-bb3b05ae3803
# ╟─17acc188-b295-4b58-a951-7e001e7bc278
# ╟─331b7442-18a4-4d41-81e3-73f289a654d0
# ╟─87b6e1a6-84f6-453c-834f-46181bb71434
# ╟─e318f2fa-6da7-40a1-b7f9-470314039cc1
# ╟─f00f4967-4579-45ee-b4e5-32cb30757e0a
# ╟─04762786-8e07-4f0a-bd5a-83fdb534cb15
# ╟─5543aa65-af9d-41f5-82cd-168c87a02450
# ╟─8c5d3217-e8cf-421a-9773-c389dd257a93
# ╟─e6582c10-28e5-4179-8021-2455ef8c58b4
