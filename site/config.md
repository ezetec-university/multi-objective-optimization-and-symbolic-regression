<!--
Add here global page variables to use throughout your website.
-->
+++
author = "Emanuele Zarfati"
mintoclevel = 2

# Add here files or directories that should be ignored by Franklin, otherwise
# these files might be copied and, if markdown, processed by Franklin which
# you might not want. Indicate directories by ending the name with a `/`.
# Base files such as LICENSE.md and README.md are ignored by default.
ignore = ["pluto_export.json", "*.jl"]

# RSS (the website_{title, descr, url} must be defined to get RSS)
generate_rss = true
website_title = "Multi-Objective Optimization and Symbolic Regression"
website_descr = "Short course"
website_url = get(ENV, "CI_PAGES_URL", "https://www.test.url")
prepath = get(ENV, "CI_PROJECT_NAME", "")
project_url = get(ENV, "CI_PROJECT_URL", "https://www.test.url")
+++

<!--
Add here global latex commands to use throughout your pages.
-->

\newcommand{\addef}[2]{
   @@definition @@title ≝ #1@@ @@content #2@@ @@}