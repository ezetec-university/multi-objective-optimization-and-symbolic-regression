@def title = "1.1 Warm-up"
@def haspluto = true

~~~
<div id="pluto-container">
   <iframe id="pluto-iframe"
      src="/notebooks/1-1-warmup/index.html" 
      style="width: 100%; height: 70vh; border: none;">
   </iframe>
</div>
~~~
