@def title = "1.2 Scalarisation Methods"
@def haspluto = true

~~~
<div id="pluto-container">
   <iframe id="pluto-iframe"
      src="/notebooks/1-2-scalarisation-methods/index.html" 
      style="width: 100%; height: 70vh; border: none;">
   </iframe>
</div>
~~~
