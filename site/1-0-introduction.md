@def title = "1.0 Introduction"
@def tags = ["definitions", "Pareto", "dominance", "optimality"]

# 1.0 Introduction

## Lack of total ordering


## Dominance

Compare solutions and objectives

\addef{Weak dominance}{
   A solution $\mathbf{x}^{\star}$ is said to **weakly dominate** another
   solution $\mathbf{x}$ if and only if the following two conditions are met:
   -  each objective function evaluated at $\mathbf{x}^{\star}$ is less than or
      equal to the corresponding objective function evaluated at $\mathbf{x}$;
   -  for at least one of the objective functions, the inequality is strictly
      less than.

   Mathematically:

   $$
   \mathbf{x}^{\star} \preceq \mathbf{x}
   \quad \Longleftrightarrow \quad 
   \begin{aligned}
   &f_i(\mathbf{x}^{\star}) \leq f_i(\mathbf{x}) \; \forall \; i
   \\
   &\exist \, j \; : \; f_j(\mathbf{x}^{\star}) < f_j(\mathbf{x}) 
   \end{aligned}
   $$
}

\addef{Strong dominance}{
   A solution $\mathbf{x}^{\star}$ is said to **strongly dominate** another
   solution $\mathbf{x}$ if and only if the following condition is met:
   -  each objective function evaluated at $\mathbf{x}^{\star}$ is less than the
      corresponding objective function evaluated at $\mathbf{x}$.

   Mathematically:

   $$
   \mathbf{x}^{\star} \prec \mathbf{x}
   \quad \Longleftrightarrow \quad 
   f_i(\mathbf{x}^{\star}) < f_i(\mathbf{x}) \; \forall \; i
   $$
}

## Optimality

\addef{Weak Pareto optimality}{
   A solution $\mathbf{x}^{\star}$ is said **weakly Pareto optimal** if there
   does not exist another solution $\mathbf{x}$ such that $\mathbf{x}^{\star}$
   is strongly dominated by $\mathbf{x}$.
}


\addef{Pareto optimality}{
   A solution $\mathbf{x}^{\star}$ is said **Pareto optimal** if there does not
   exist another solution $\mathbf{x}$ such that $\mathbf{x}^{\star}$ is weakly
   dominated by $\mathbf{x}$.
}

\addef{Proper Pareto optimality (in the sense of Geoffrion)}{ 
   The concept's genesis is attributed to [(Kuhn and Tucker,
   1951)](https://doi.org/10.1007/978-3-0348-0439-4_11) who, in the context of
   maximisation of multiple functions, noted how some solutions have undesirable
   properties. 
   
   [(Geoffrion, 1968)](https://doi.org/10.1016/0022-247X(68)90201-1) elaborated
   the concept further (talking about efficiency instead of Pareto optimality). 
   A solution $\mathbf{x}^{\star}$ is **properly Pareto optimal** if it is
   Pareto optimal and if there exists at least one pair of objectives, $i$ and
   $j$, for which a finite reduction in objective $i$ evaluated at $\mathbf{x}$
   is feasible, but only at the cost of a reasonable increase in the objective
   $j$.

   Mathematically, for some arbitrary $M$:

   $$
   \frac{f_i(\mathbf{x}^{\star}) - f_i(\mathbf{x})}{f_j(\mathbf{x}) - f_j(\mathbf{x}^{\star})} \le M
   $$
}