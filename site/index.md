@def title = "Multi-Objective Optimization and Symbolic Regression"
@def tags = ["optimization", "julia"]

# Multi-Objective Optimization and Symbolic Regression

\tableofcontents <!-- you can use \toc as well -->

This site contains the notes of the course _Multi-objective Optimization and
Symbolic Regression_ held at the University of Modena and Reggio Emilia in
February 2024 within the PhD Programme in Computer and Data Science for
Technological and Social Innovation.
